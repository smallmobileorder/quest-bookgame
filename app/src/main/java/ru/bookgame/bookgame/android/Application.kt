package ru.bookgame.bookgame.android

import android.app.Application
import android.content.Context
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.AudioTimestamp
import android.media.MediaPlayer
import com.android.vending.billing.IInAppBillingService
import ru.bookgame.bookgame.R

class Application : Application() {

    val defaultMusic = R.raw.fon_main
    var timeStart: Long = System.currentTimeMillis()

    lateinit var inAppService : IInAppBillingService

    private val voices = ArrayList<Pair<MediaPlayer, Int>>(10)



    fun changeThemeMusic(id: Int = defaultMusic) {
        if (voices.isNotEmpty())
            stopMusic()
        startMusic(id, true)
    }

    fun startMusic(id: Int = defaultMusic, isTheme: Boolean = false) {
        val pref = getSharedPreferences(getString(R.string.is_play_music), Context.MODE_PRIVATE)
        val isMusicale = pref.getBoolean(getString(R.string.is_play_music), true)
        val isSound = pref.getBoolean(getString(R.string.is_play_sound), true)

        val voice = MediaPlayer.create(this, id)
        voice.isLooping = isTheme

        if ((!isMusicale && isTheme) || (!isSound && !isTheme)) {
            voice.setVolume(0f, 0f)
        }
        val music = Pair(voice, id)
        voice!!.setOnCompletionListener {
            voices.remove(music)
            voice.release()
        }
        voice.start()
        voices.add(music)
    }


    fun stopMusic() {
        for (mus in voices) {
            if (mus.first.isPlaying)
                mus.first.stop()
            mus.first.release()
        }
        voices.clear()
    }

    fun decodeSampledBitmapFromResource(res: Resources, resId: Int, reqWidth: Int, reqHeight: Int): Bitmap {
        // First decode with inJustDecodeBounds=true to check dimensions
        val options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeResource(res, resId, options)

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight)

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false
        return BitmapFactory.decodeResource(res, resId, options)
    }

    private fun calculateInSampleSize(options: BitmapFactory.Options, reqWidth: Int, reqHeight: Int): Int {
        // Raw height and width of image
        val height = options.outHeight
        val width = options.outWidth
        var inSampleSize = 1

        if (height > reqHeight || width > reqWidth) {

            val halfHeight = height / 2
            val halfWidth = width / 2

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while (halfHeight / inSampleSize >= reqHeight && halfWidth / inSampleSize >= reqWidth) {
                inSampleSize *= 2
            }
        }

        return inSampleSize
    }

    fun pxFromDp(dp: Float): Float {
        return dp * this.resources.displayMetrics.density
    }


}