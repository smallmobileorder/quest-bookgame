package ru.bookgame.bookgame.android

import android.content.Context
import android.text.Layout
import android.util.AttributeSet

class CustomTextView(context: Context, attrs: AttributeSet) : androidx.appcompat.widget.AppCompatTextView(context, attrs) {

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)

        val width = Math.ceil(getMaxLineWidth(layout).toDouble()).toInt()
        val height = measuredHeight
        setMeasuredDimension(width, height)
    }

    private fun getMaxLineWidth(layout: Layout): Float {
        var maximumWidth = 0.0f
        val lines = layout.lineCount
        if (lines == 1){
            maximumWidth = Math.max(layout.getLineWidth(0) * 1.14f, maximumWidth)
        }
        else {
            for (line in 0 until lines) {
                maximumWidth = Math.max(layout.getLineWidth(line) * 1.08f, maximumWidth)
            }
        }

        return maximumWidth
    }
}