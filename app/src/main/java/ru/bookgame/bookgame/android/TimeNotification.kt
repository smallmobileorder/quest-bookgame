package ru.bookgame.bookgame.android

import android.app.AlarmManager
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import ru.bookgame.bookgame.R
import ru.bookgame.bookgame.android.activity.MainActivity
import java.util.*
import java.util.concurrent.TimeUnit


class TimeNotification : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        val notificationManager =
                context!!.getSystemService(AppCompatActivity.NOTIFICATION_SERVICE) as NotificationManager
        val resultIntent = Intent(context, MainActivity::class.java)
        val resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT)
        val builder = NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.icon_app)
                .setContentTitle("Книга игр")
                .setContentText("Привет, Путник! Приключения ждут тебя.")
                .setContentIntent(resultPendingIntent)
                .setAutoCancel(true)
        if (Locale.getDefault().language == "en") {
            builder.setContentTitle("Book Game")
                    .setContentText("Hello, Traveler! Adventures await you.")
        }
        val notification = builder.build()
        notificationManager.notify(1, notification)

//        val timePush = System.currentTimeMillis() + 20*1000
        val timePush = System.currentTimeMillis() + 259200000L
        val hours = TimeUnit.MILLISECONDS.toHours(timePush)
        val minutes = TimeUnit.MILLISECONDS.toMinutes(timePush)
        val startDay = timePush - (hours * 60 * 60 * 1000 + minutes * 60 * 1000)
        val timePushCalculated = startDay + (16 * 60 * 60 * 1000 + 20 * 60 * 1000..20 * 60 * 60 * 1000 + 40 * 60 * 1000).random()

        val am = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val pendingIntent = PendingIntent.getBroadcast(context, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT)
        am.cancel(pendingIntent)
        am.set(AlarmManager.RTC_WAKEUP, timePushCalculated, pendingIntent)
    }
}