package ru.bookgame.bookgame.android

import android.app.AlarmManager
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import ru.bookgame.bookgame.R
import ru.bookgame.bookgame.android.activity.MainActivity
import java.util.*

class TimeNotification1 : BroadcastReceiver() {

    override fun onReceive(context: Context?, intent: Intent?) {
        val notificationManager =
                context!!.getSystemService(AppCompatActivity.NOTIFICATION_SERVICE) as NotificationManager
        val resultIntent = Intent(context, MainActivity::class.java)
        val resultPendingIntent = PendingIntent.getActivity(context, 0, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT)
        val builder2 = NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.icon_app)
                .setContentTitle("Книга игр")
                .setContentText("Надеюсь, у тебя все хорошо.")
                .setContentIntent(resultPendingIntent)
                .setAutoCancel(true)
        val notification2 = builder2.build()
        notificationManager.notify(2, notification2)
        if (Locale.getDefault().language == "en") {
            builder2.setContentTitle("Book Game")
                    .setContentText("Hope you're doing well.")
        }

//        var timePush11 = System.currentTimeMillis() + 2*60*1000
        var timePush1 = System.currentTimeMillis() + 1209600000L
        val c = Calendar.getInstance()
        c.timeInMillis = timePush1
        val dayOfWeek = c.get(Calendar.DAY_OF_WEEK)
        if (dayOfWeek==1 || dayOfWeek==7)
            timePush1 += 2 * 24 * 60 * 60 * 1000
        val am = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val pendingIntent = PendingIntent.getBroadcast(context, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT)

        am.cancel(pendingIntent)
        am.set(AlarmManager.RTC_WAKEUP, timePush1, pendingIntent)
    }
}