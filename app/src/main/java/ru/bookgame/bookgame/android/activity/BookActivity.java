package ru.bookgame.bookgame.android.activity;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Environment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Locale;

import ru.bookgame.bookgame.R;
import ru.bookgame.bookgame.android.Application;
import ru.bookgame.bookgame.android.adapter.BookRecyclerAdapter;
import ru.bookgame.bookgame.android.adapter.LayoutClickListener;
import ru.bookgame.bookgame.book.BookProperty;
import ru.bookgame.bookgame.book.Paragraph;
import ru.bookgame.bookgame.parser.BookParser;

import static androidx.recyclerview.widget.LinearLayoutManager.VERTICAL;

public class BookActivity extends AppCompatActivity {

    private static final String APP_PREFERENCES = "Book_game_preferences";

    private String title;
    private int fileResID;
    private int backgroundResID;

    private int musicID;
    private boolean changeAnswerVisibility = false;
    private RecyclerView recyclerView;
    private String language = "en";
    private SharedPreferences sharedPreferences;
    private String KEY_TO_PREFERENCES;

    private BookRecyclerAdapter bookRecyclerAdapter;
    private boolean changeAnswer = false;
    private boolean isPause = false;

    private String citat;
    private String autorCitat;
    private LottieAnimationView animTap;
    private String name = "";
    public String path = "";
    private int id = -1;

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);
        ConstraintLayout background = findViewById(R.id.activity_book_load);
        title = getIntent().getStringExtra("title");
        fileResID = getIntent().getIntExtra("file", -1);
        backgroundResID = getIntent().getIntExtra("image", -1);
        musicID = getIntent().getIntExtra("music", R.raw.fon_main);
        changeAnswer = getIntent().getBooleanExtra("change", false);
        int backgroundProgress = getIntent().getIntExtra("progress_background", -1);
        citat = getIntent().getStringExtra("citat");
        autorCitat = getIntent().getStringExtra("autorCitat");
        language = Locale.getDefault().getLanguage();
        background.setBackground(getDrawable(backgroundProgress));


        if (fileResID == -1) {
            if (language.equals("ru"))
                Toast.makeText(this, "Книга отсутствует!", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(this, "Book is missing!", Toast.LENGTH_SHORT).show();
        }
        KEY_TO_PREFERENCES = title;
        sharedPreferences = getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE);

        new AsyncTask<Void, Void, Void>() {

            private boolean isFirst;
            private FrameLayout background;
            private ConstraintLayout progressBar;
            private LottieAnimationView animLoading;
            private BitmapDrawable bitmapDrawable;

            @SuppressLint("WrongThread")
            @Override
            protected Void doInBackground(Void... voids) {
                background = findViewById(R.id.activity_book_main);
                progressBar = findViewById(R.id.activity_book_load);
                animLoading = findViewById(R.id.anim_book_loading);
                animTap = findViewById(R.id.anim_book_tap);
                animLoading.setVisibility(View.VISIBLE);
                animLoading.setAnimation(R.raw.loading_book_animation);
                animLoading.playAnimation();
                animLoading.loop(true);
                if (backgroundResID == -1) {
                    background.setBackgroundColor(Color.BLACK);
                } else {
                    bitmapDrawable = new BitmapDrawable(((Application) getApplication()).decodeSampledBitmapFromResource(getResources(), backgroundResID, 200, 200));
                }
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                isFirst = initRecycler();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                animTap.setVisibility(View.GONE);
                progressBar.setAlpha(1f);
                animLoading.animate().alpha(0f).setDuration(500).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        animLoading.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        animLoading.cancelAnimation();
                        animLoading.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                    }
                });
                progressBar.animate().alpha(0f).setDuration(500).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        background.setBackground(bitmapDrawable);
                        background.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        progressBar.setVisibility(View.GONE);
                        if (isFirst) {
                            animTap.setVisibility(View.VISIBLE);
                            animTap.setAnimation(R.raw.touch_animation);
                            animTap.playAnimation();
                            animTap.loop(true);
                        }
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {
                    }
                });
                setListenerToRecycler();
            }
        }.execute();

        ImageView menuBtn = findViewById(R.id.activity_book_btn_menu);
        menuBtn.setOnClickListener(v -> {
            ((Application) getApplicationContext()).startMusic(R.raw.klik, false);
            Intent intent = new Intent(BookActivity.this, SettingsActivity.class);
            intent.putExtra("openEnds", new Gson().toJson(bookRecyclerAdapter.getBookProperty().getOpenEnds()));
            intent.putExtra("openCountEnds", bookRecyclerAdapter.getBookProperty().getOpenEnds().size());
            intent.putExtra("countEnds", bookRecyclerAdapter.getBookProperty().getCountEnds());
            intent.putExtra("image", backgroundResID);
            intent.putExtra("citat", citat);
            intent.putExtra("autorCitat", autorCitat);
            intent.putExtra("file", fileResID);
            intent.putExtra("change", changeAnswer);
            intent.putExtra("music", musicID);
            isPause = true;
            startActivityForResult(intent, 1);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        int id = this.id;
        String name = this.name;
        if (requestCode == 1011 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            new TaskImage(true, name, id).execute();

        }
        if (requestCode == 1012 && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            new TaskImage(false, name, id).execute();
        }
    }

    public void saveImage(String name, int id) {
        new TaskImage1(name, id).execute();
    }

    public void setImageData(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public void resultWrite(String path) {
        Intent share = new Intent(Intent.ACTION_SEND);
        // Set the MIME type
        share.setType("image/*");
        // Create the URI from the media
        File media = new File(path);
        Uri uri = Uri.fromFile(media);
        // Add the URI to the Intent.
        share.putExtra(Intent.EXTRA_STREAM, uri);
        share.setPackage("com.facebook.katana");
        // Broadcast the Intent.
        if (share.resolveActivity(getPackageManager()) != null) {
            startActivity(Intent.createChooser(share, "Share to"));
        } else {
            showDialogApp("Facebook");
        }
    }

    public void showDialogApp(String name) {
        String msg;
        if (Locale.getDefault().getLanguage().equals("ru"))
            msg = "Приложение " + name + " отсутствует";
        else msg = "The application " + name + " not found";
        new AlertDialog.Builder(this)
                .setMessage(msg)
                .setPositiveButton("OK", (dialog, e) -> dialog.cancel())
                .show();
    }

    public void resultWrite1(String path) {
        Intent share = new Intent(Intent.ACTION_SEND);
        // Set the MIME type
        share.setType("image/*");
        // Create the URI from the media
        File media = new File(path);
        Uri uri = Uri.fromFile(media);
        // Add the URI to the Intent.
        share.putExtra(Intent.EXTRA_STREAM, uri);
        share.setPackage("com.instagram.android");
        // Broadcast the Intent.
        if (share.resolveActivity(getPackageManager()) != null) {
            startActivity(Intent.createChooser(share, "Share to"));
        } else {
            showDialogApp("Instagram");
        }
    }

    public void ckickSound() {
        Application app = (Application) getApplication();
        app.startMusic(R.raw.klik, false);
    }

    class TaskImage1 extends AsyncTask<Void, Void, String> {
        String name;
        int id;

        TaskImage1(String name, int id) {
            this.name = name;
            this.id = id;
        }

        @Override
        protected String doInBackground(Void... voids) {
            String mediaPath = Environment.getExternalStorageDirectory().toString();
            String path = mediaPath + "/" + name + ".PNG";
            File media1 = new File(path);
            if (!media1.exists()) {
                path = saveExternalImage(name, id);
            }
            if (path.isEmpty())
                path = mediaPath + "/" + name + ".PNG";

            Log.i("SAVE_INSTAGRAM", "path = " + path);
            return path;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            path = s;
        }
    }

    class TaskImage extends AsyncTask<Void, Void, String> {
        boolean isInsta;
        String name;
        int id;

        TaskImage(boolean isInsta, String name, int id) {
            this.isInsta = isInsta;
            this.name = name;
            this.id = id;
        }

        @Override
        protected String doInBackground(Void... voids) {
            String mediaPath = Environment.getExternalStorageDirectory().toString();
            String path = mediaPath + "/" + name + ".PNG";
            File media1 = new File(path);
            if (!media1.exists()) {
                path = saveExternalImage(name, id);
            }
            if (path.isEmpty())
                path = mediaPath + "/" + name + ".PNG";
            return path;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            if (isInsta)
                resultWrite1(aVoid);
            else
                resultWrite(aVoid);
        }
    }

    public String saveExternalImage(String name, int resource) {
        FileOutputStream outStream;
        String path = "";
        try {
            Bitmap bm = BitmapFactory.decodeResource(getResources(), resource);

            String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
            File file = new File(extStorageDirectory, name + ".PNG");
            path = file.getPath();
            outStream = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.PNG, 100, outStream);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return path;
    }

    private boolean initRecycler() {
        recyclerView = findViewById(R.id.activity_book_recycler);
        recyclerView.setLayoutManager(new SpeedyLinearLayoutManager(this, VERTICAL, false));
        boolean isFirst = false;
        BookProperty bookProperty = null;
        if (sharedPreferences.contains(KEY_TO_PREFERENCES)) {
            try {
                bookProperty = new Gson().fromJson(sharedPreferences.getString(KEY_TO_PREFERENCES, ""), BookProperty.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
        }
        if (bookProperty == null) {
            bookProperty = new BookProperty(fileResID, title, citat, autorCitat, backgroundResID);
            BookParser.parse(this, bookProperty);
            isFirst = true;
        }
        List<Paragraph> paragraphs = bookProperty.getCurrentParagraphList();
        if (paragraphs == null || paragraphs.size() == 0) {
            isFirst = true;
        }
        bookRecyclerAdapter = new BookRecyclerAdapter(this, recyclerView, bookProperty);
        recyclerView.setAdapter(bookRecyclerAdapter);
        DividerItemDecoration itemDecorator = new DividerItemDecoration(this, DividerItemDecoration.VERTICAL);
        Drawable drawable = ContextCompat.getDrawable(this, R.drawable.recycler_divider);
        if (drawable != null)
            itemDecorator.setDrawable(drawable);
        recyclerView.addItemDecoration(itemDecorator);
        return isFirst;
    }

    private void setListenerToRecycler() {
        recyclerView.addOnItemTouchListener(new LayoutClickListener(this, recyclerView,
                () -> {
                    if (fileResID == -1)
                        return;
                    if (animTap.isAnimating()) animTap.cancelAnimation();
                    animTap.setVisibility(View.GONE);
                    bookRecyclerAdapter.nextParagraph();
                }));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            bookRecyclerAdapter.notifyDataSetChanged();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isPause)
            ((Application) getApplication()).changeThemeMusic(musicID);
        isPause = false;
    }

    @Override
    public void onPause() {
        if (!isPause)
            ((Application) getApplication()).stopMusic();
        super.onPause();
    }

    @Override
    protected void onStop() {
        if (bookRecyclerAdapter != null) {
            BookProperty bookProperty = bookRecyclerAdapter.getBookProperty();
            if (bookProperty != null) {
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString(KEY_TO_PREFERENCES, new Gson().toJson(bookProperty));
                editor.apply();
            }
        }
        super.onStop();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (data == null)
            return;
        if (data.getBooleanExtra("restart", false)) {
            bookRecyclerAdapter.restartGame();
            ((Application) getApplication()).changeThemeMusic(musicID);
            isPause = false;
        }
        if (data.getBooleanExtra("changeBtn", false)) {
            bookRecyclerAdapter.getBookActivity().changeAnswerVisibility(true);
            bookRecyclerAdapter.notifyDataSetChanged();
        }
        if (requestCode == MainActivity.REQUEST_CODE_BUY) {
            int responseCode = data.getIntExtra("RESPONSE_CODE", -1);
            if (responseCode == MainActivity.BILLING_RESPONSE_RESULT_OK) {
                String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
                readPurchase(purchaseData);
            }
        }
    }

    public void purchaseProduct(MainActivity.InAppProduct product) throws Exception {
        String sku = product.getSku();
        String type = product.getType();
        Bundle buyIntentBundle = ((Application) getApplication()).getInAppService().getBuyIntent(
                3, getPackageName(),
                sku, type, null);
        PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");
        startIntentSenderForResult(pendingIntent.getIntentSender(),
                MainActivity.REQUEST_CODE_BUY, new Intent(), 0, 0,
                0, null);
    }

    int targetPositionRec = 0;

    public void setTargetPosition(int targetPosition) {
        this.targetPositionRec = targetPosition;
    }

    public void openFinal() {
        Intent intent = new Intent(this, EndsActivity.class);
        intent.putExtra("openEnds", new Gson().toJson(bookRecyclerAdapter.getBookProperty().getOpenEnds()));
        intent.putExtra("openCountEnds", bookRecyclerAdapter.getBookProperty().getOpenEnds().size());
        intent.putExtra("countEnds", bookRecyclerAdapter.getBookProperty().getCountEnds());
        intent.putExtra("image", backgroundResID);
        intent.putExtra("citat", citat);
        intent.putExtra("autorCitat", autorCitat);
        intent.putExtra("file", fileResID);
        intent.putExtra("change", changeAnswer);
        intent.putExtra("music", musicID);
        isPause = true;
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public void changeAnswerVisibility(boolean b) {
        changeAnswerVisibility = b;
    }

    public boolean getChangeAnswerVisibility() {
        return changeAnswerVisibility;
    }

    public int getBookId() {
        return fileResID;
    }


    class SpeedyLinearLayoutManager extends LinearLayoutManager {

        @Override
        public void smoothScrollToPosition(RecyclerView recyclerView, RecyclerView.State state, int position) {
            LinearSmoothScroller linearSmoothScroller = new LinearSmoothScroller(recyclerView.getContext()) {
                @Override
                protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                    return 250f / displayMetrics.densityDpi / Math.abs(targetPositionRec - position);
                }
            };
            linearSmoothScroller.setTargetPosition(position);
            startSmoothScroll(linearSmoothScroller);

        }

        SpeedyLinearLayoutManager(Context context, int orientation, boolean reverseLayout) {
            super(context, orientation, reverseLayout);
        }
    }


    private void readPurchase(String purchaseData) {
        try {
            JSONObject jsonObject = new JSONObject(purchaseData);
            String productId = jsonObject.getString("productId");
            String product3 = MainActivity.PRODUCT3;
            if (product3.equals(productId)) {
                changeAnswer = true;
                bookRecyclerAdapter.notifyDataSetChanged();
                getSharedPreferences(MainActivity.SHARED_KEY, Context.MODE_PRIVATE).edit().putBoolean(MainActivity.CHANGE_ANSWER, true).apply();
                if (language.equals("ru")) {
                    Toast.makeText(this, "Поздравляем!", Toast.LENGTH_SHORT).show();
                    Toast.makeText(this, "Вы открыли возможность изменять ответ", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Congratulation!", Toast.LENGTH_SHORT).show();
                    Toast.makeText(this, "You opened option change answer", Toast.LENGTH_SHORT).show();
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
