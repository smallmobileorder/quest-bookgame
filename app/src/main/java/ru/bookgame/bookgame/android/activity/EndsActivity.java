package ru.bookgame.bookgame.android.activity;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import ru.bookgame.bookgame.R;
import ru.bookgame.bookgame.android.Application;
import ru.bookgame.bookgame.android.adapter.EndsListAdapter;
import ru.bookgame.bookgame.book.Paragraph;

public class EndsActivity extends AppCompatActivity {

    private boolean isPause = true;

    @SuppressLint("StaticFieldLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ends);

        new AsyncTask<Void, Void, Void>() {

            private Map<Integer, Paragraph> openEnds = null;
            private ListView listView;
            private FrameLayout progress;
            private LinearLayout background;
            private BitmapDrawable bitmapDrawable;

            @SuppressLint("WrongThread")
            @Override
            protected Void doInBackground(Void... voids) {
                background = findViewById(R.id.activity_ends_main);
                progress = findViewById(R.id.activity_end_load);
                bitmapDrawable = new BitmapDrawable(((Application) getApplication()).decodeSampledBitmapFromResource(getResources(), getIntent().getIntExtra("image", 0), 100, 200));

                Type type = new TypeToken<Map<Integer, Paragraph>>() {
                }.getType();
                openEnds = new Gson().fromJson(getIntent().getStringExtra("openEnds"), type);
                listView = findViewById(R.id.activity_end_list_ends);
                listView.setAdapter(new EndsListAdapter(EndsActivity.this, openEnds));
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                progress.setAlpha(1f);
                progress.animate().alpha(0f).setDuration(500).setListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        background.setBackground(bitmapDrawable);
                        background.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        progress.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                });
            }
        }.execute();

        ImageButton backBtn = findViewById(R.id.activity_ends_btn_back);
        backBtn.setOnClickListener(v -> {
            onBackPressed();
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isPause)
            ((Application) getApplication()).changeThemeMusic(getIntent().getIntExtra("music", 1));
        isPause = false;
    }

    @Override
    public void onPause() {
        if (!isPause)
            ((Application) getApplication()).stopMusic();
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        isPause = true;
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}
