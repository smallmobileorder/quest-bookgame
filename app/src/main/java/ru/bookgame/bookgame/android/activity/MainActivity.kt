package ru.bookgame.bookgame.android.activity

import android.animation.Animator
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.PendingIntent
import android.content.*
import android.content.res.Configuration
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import com.airbnb.lottie.LottieAnimationView
import com.android.vending.billing.IInAppBillingService
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_main.*
import org.json.JSONObject
import ru.bookgame.bookgame.BuildConfig
import ru.bookgame.bookgame.R
import ru.bookgame.bookgame.android.Application
import ru.bookgame.bookgame.android.TimeNotification
import ru.bookgame.bookgame.android.TimeNotification1
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit


class MainActivity : AppCompatActivity() {

    private var sharedCount = 0
    private var stateTimer = StateTimer.DONT_START
    private lateinit var okButton: Button
    private lateinit var timerText: TextView
    private lateinit var check1: LottieAnimationView
    private lateinit var check2: LottieAnimationView
    private lateinit var check3: LottieAnimationView
    private lateinit var check22: TextView

    private lateinit var scheduledExecutorServiceData: ScheduledExecutorService
    private lateinit var scheduledExecutorServiceTimer: ScheduledExecutorService
    private lateinit var noteTask: NoteTask
    private lateinit var timerTask: TimerDialogTask
    private lateinit var language: String
    /**
     * fields
     */
    private var app: Application? = null
    private var openingBook2 = false
    private var openingBook3 = false
    private var openingBook4 = false
    private var openingBook5 = false
    private var openingNotes = false
    private var timer = 0

    internal var inAppBillingService: IInAppBillingService? = null

    private var serviceConnection: ServiceConnection? = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName, service: IBinder) {
            inAppBillingService = IInAppBillingService.Stub.asInterface(service)
            app!!.inAppService = inAppBillingService!!
            try {
                readMyPurchases()
                purchases = getInAppPurchases(PRODUCT1, PRODUCT2, PRODUCT3, PRODUCT4, PRODUCT5)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

        override fun onServiceDisconnected(name: ComponentName) {
            inAppBillingService = null
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        app = this.application as Application
        loadLocal()
        setContentView(R.layout.activity_main)
        initFirebase()
        app!!.startMusic(R.raw.fon_main, true)
        app!!.timeStart = System.currentTimeMillis()
        initPreferences()
        initPurchaseService()
        updateImages()
        hidePopView()
        initListeners()
        restartNotify()

    }


    private fun notes() {
        val lastNotesTime = getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE).getLong("LAST_NOTES", System.currentTimeMillis())
        if (System.currentTimeMillis() - lastNotesTime > 7 * 24 * 60 * 60 * 1000) openingNotes = true
//        if (System.currentTimeMillis() - lastNotesTime > 10 * 1000) openingNotes = true
        noteTask = NoteTask()
        timerTask = TimerDialogTask()
        scheduledExecutorServiceData = Executors.newSingleThreadScheduledExecutor()
        scheduledExecutorServiceTimer = Executors.newSingleThreadScheduledExecutor()
        scheduledExecutorServiceData.schedule(noteTask, 1, TimeUnit.MINUTES)
//        scheduledExecutorServiceData.schedule(noteTask, 5, TimeUnit.SECONDS)
    }

    inner class NoteTask : TimerTask() {
        override fun run() {
            if (System.currentTimeMillis() - (application as Application).timeStart > 30 * 60 * 1000) {
//            if (System.currentTimeMillis() - (application as Application).timeStart > 10 * 1000) {
                showDialogNote()
                noteTask.cancel()
                scheduledExecutorServiceData.shutdownNow()
            } else {
                Log.i("NOTE_TASK", "if2")
                if (!scheduledExecutorServiceData.isShutdown)
                    scheduledExecutorServiceData.schedule(noteTask, 5, TimeUnit.MINUTES)
            }
        }
    }

    private fun showDialogNote() {
        runOnUiThread {
            val builder = AlertDialog.Builder(this)
            val dialogView = View.inflate(this, R.layout.custom_dialog, null)
            builder.setView(dialogView)
                    .setView(dialogView)
                    .setCancelable(true)
            val alert = builder.create()

            val text = dialogView.findViewById<TextView>(R.id.text_custom_dialog)
            val ok = dialogView.findViewById<Button>(R.id.buy_button)
            val cancel = dialogView.findViewById<Button>(R.id.cancel_button)
            ok.setOnClickListener {
                (applicationContext as Application).startMusic(R.raw.klik, false)
                alert.cancel()
                showNote()
            }
            cancel.setOnClickListener {
                (applicationContext as Application).startMusic(R.raw.klik, false)
                alert.cancel()

            }
            if (language == "ru") {
                text.text = "Привет, Путник! Можно поделиться мыслями?"
                ok.text = "Да"
                cancel.text = "Нет"
            } else {
                text.text = "Hi there, Traveler! Can we share some thoughts?"
                ok.text = "Yes"
                cancel.text = "No"
            }
            alert.show()
        }
    }

    private fun showNote() {
        getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE).edit().putLong("LAST_NOTES", System.currentTimeMillis()).apply()

        val count = getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE).getInt("COUNT_NOTE", 0)
        val arrayStringNotes = resources.getStringArray(R.array.notes)
        if (count < arrayStringNotes.size - 1) {
            val string = arrayStringNotes[count]
            val builder = AlertDialog.Builder(this)
            val dialogView = View.inflate(this, R.layout.custom_dialog_free, null)
            builder.setView(dialogView)
                    .setView(dialogView)
                    .setCancelable(true)
            val alert = builder.create()

            val text = dialogView.findViewById<TextView>(R.id.text_custom_dialog)
            val ok = dialogView.findViewById<Button>(R.id.buy_button)
            ok.setOnClickListener {
                (applicationContext as Application).startMusic(R.raw.klik, false)
                alert.cancel()
            }
            text.text = string
            if (language == "ru")
                ok.text = "Хорошо"
            else {
                ok.text = "Ok"
            }
            alert.show()
            getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE).edit().putInt("COUNT_NOTE", count + 1).apply()
        }

    }

    override fun onStart() {
        notes()
        if (stateTimer == StateTimer.RUNING) {
            timerTask = TimerDialogTask()
            scheduledExecutorServiceTimer = Executors.newSingleThreadScheduledExecutor()
            scheduledExecutorServiceTimer.schedule(timerTask, 1, TimeUnit.SECONDS)
        }
        Log.i("TIMER_TASK_DEBUG_TEST", "onstart state = " + stateTimer)
        super.onStart()
    }

    override fun onStop() {
        noteTask.cancel()
        scheduledExecutorServiceData.shutdownNow()
        if (!scheduledExecutorServiceTimer.isShutdown) {
            timerTask.cancel()
            scheduledExecutorServiceTimer.shutdownNow()
        }
        super.onStop()
    }

    private fun restartNotify() {
//        val timePush = System.currentTimeMillis()
        val timePush = System.currentTimeMillis() + 72 * 60 * 60 * 1000
        val date = Date(timePush)
        val hours = date.hours
        val minutes = date.minutes
        val startDay = timePush - (hours * 60 * 60 * 1000 + minutes * 60 * 1000)
//        val timePushCalculated = timePush + (20 * 1000)
        val timePushCalculated = startDay + (16 * 60 * 60 * 1000 + 20 * 60 * 1000..20 * 60 * 60 * 1000 + 40 * 60 * 1000).random()

        val dateTest = Date(timePushCalculated)
        val format = SimpleDateFormat("yyyy.MM.dd HH:mm")
        Log.i("TIME_DATE", "result = " + format.format(dateTest))

        var timePush1 = System.currentTimeMillis() + 14*24*60*60*1000
//        var timePush1 = System.currentTimeMillis() + 2 * 60 * 1000
        val c = Calendar.getInstance()
        c.timeInMillis = timePush1
        val dayOfWeek = c.get(Calendar.DAY_OF_WEEK)
        if (dayOfWeek == 1 || dayOfWeek == 7)
            timePush1 += 2 * 24 * 60 * 60 * 1000
        val date1 = Date(timePush1)
        val hours1 = date1.hours
        val minutes1 = date1.minutes
        val startDay1 = timePush1 - (hours1 * 60 * 60 * 1000 + minutes1 * 60 * 1000)
        val timePushCalculated1 = startDay1 + (15 * 60 * 60 * 1000)

        val dateTest1 = Date(timePushCalculated1)
        val format1 = SimpleDateFormat("yyyy.MM.dd HH:mm")
        Log.i("TIME_DATE", "result2 = " + format1.format(dateTest1))

        val am = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val intent = Intent(this, TimeNotification::class.java)
        val pendingIntent = PendingIntent.getBroadcast(this, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT)
        am.cancel(pendingIntent)
        am.set(AlarmManager.RTC_WAKEUP, timePushCalculated, pendingIntent)


        val intent1 = Intent(this, TimeNotification1::class.java)
        val pendingIntent1 = PendingIntent.getBroadcast(this, 0,
                intent1, PendingIntent.FLAG_CANCEL_CURRENT)
        am.cancel(pendingIntent1)
        am.set(AlarmManager.RTC_WAKEUP, timePushCalculated1, pendingIntent1)
    }


    private fun initFirebase() {
        val database = FirebaseDatabase.getInstance()
        val myRef = database.getReference("/key")
        val linearLayout = findViewById<LinearLayout>(R.id.activity_main_main)
        myRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                val value = dataSnapshot.getValue(Boolean::class.java)
                if (value != null) {
                    if (value) {
                        linearLayout.visibility = View.GONE
                    } else {
                        linearLayout.visibility = View.VISIBLE
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {}
        })
    }


    private fun initPreferences() {
        openingBook2 = getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE).getBoolean(SHARED_BOOK1, false)
        openingBook3 = getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE).getBoolean(SHARED_BOOK2, false)
        openingBook4 = getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE).getBoolean(SHARED_BOOK3, false)
        openingBook5 = getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE).getBoolean(SHARED_BOOK4, false)
        changeAnswer = getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE).getBoolean(CHANGE_ANSWER, false)
    }


    @Throws(Exception::class)
    private fun readMyPurchases() {
        val result = inAppBillingService!!.getPurchases(
                3, packageName, "inapp", null)

        if (result.getInt("RESPONSE_CODE", -1) != 0) {
            throw Exception("Invalid response code")
        }
        val responseList = result.getStringArrayList("INAPP_PURCHASE_DATA_LIST")

        for (purchaseData in responseList!!) {
            val jsonObject = JSONObject(purchaseData)
            val purchaseState = jsonObject.getInt("purchaseState")
            if (purchaseState == PURCHASE_STATUS_PURCHASED) {
                when (jsonObject.getString("productId")) {
                    PRODUCT1 -> {
                        openingBook2 = true
                        getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE).edit().putBoolean(SHARED_BOOK1, true).apply()
                    }
                    PRODUCT2 -> {
                        openingBook3 = true
                        getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE).edit().putBoolean(SHARED_BOOK2, true).apply()
                    }
                    PRODUCT3 -> {
                        changeAnswer = true
                        getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE).edit().putBoolean(CHANGE_ANSWER, true).apply()
                    }
                    PRODUCT4 -> {
                        openingBook4 = true
                        getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE).edit().putBoolean(SHARED_BOOK3, true).apply()
                    }
                    PRODUCT5 -> {
                        openingBook5 = true
                        getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE).edit().putBoolean(SHARED_BOOK4, true).apply()
                    }
                }
                refreshAfterBuying()
            }
        }
    }

    private fun initPurchaseService() {
        val serviceIntent = Intent("com.android.vending.billing.InAppBillingService.BIND")
        serviceIntent.setPackage("com.android.vending")
        bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE)
    }

    private fun refreshAfterBuying() {
        if (openingBook2) {
            activity_main_book2_lock!!.visibility = View.GONE
        }
        if (openingBook3) {
            activity_main_book3_lock!!.visibility = View.GONE
        }
    }

    private fun initListeners() {
        activity_main_btn_shared.setOnClickListener {
            (applicationContext as Application).startMusic(R.raw.klik, false)
            this.sharedFunction(it)
        }
        activity_main_btn_good_deal!!.setOnClickListener {
            (applicationContext as Application).startMusic(R.raw.klik, false)
            this.markedAppInGooglePlay(it)
        }
        activity_main_btn_community!!.setOnClickListener {
            (applicationContext as Application).startMusic(R.raw.klik, false)
            this.communityOpenLink(it)
        }
        subscribe_on_news!!.setOnClickListener {
            (applicationContext as Application).startMusic(R.raw.klik, false)
            this.sendEmail()
        }
        activity_main_book1.setOnClickListener { v ->
            (applicationContext as Application).startMusic(R.raw.klik, false)

            if (language == "ru")
                changeActivity("Бюрократия", R.raw.buro_rus, R.drawable.buro_back, R.raw.fon_burokratia, R.drawable.s_buro_cover_rus, "Бюрократизм является неизбежным элементом всякой государственности.", "Лев Троцкий")
            else
                changeActivity("Bureaucracy", R.raw.buro_eng, R.drawable.buro_back, R.raw.fon_burokratia, R.drawable.s_buro_cover_eng, "Bureaucracy is an inevitable element of any state.", "Leon Trotsky")

        }
        activity_main_book2.setOnClickListener {
            (applicationContext as Application).startMusic(R.raw.klik, false)
            openBook2()
        }
        activity_main_book3.setOnClickListener {
            (applicationContext as Application).startMusic(R.raw.klik, false)
            openBook3()
        }
        activity_main_book4.setOnClickListener {
            (applicationContext as Application).startMusic(R.raw.klik, false)
            openBook4()
        }
        activity_main_book5.setOnClickListener {
            (applicationContext as Application).startMusic(R.raw.klik, false)
            openBook5()
        }
        restore_purchases!!.setOnClickListener {
            (applicationContext as Application).startMusic(R.raw.klik, false)
        }

        if (!openingBook2) {
            activity_main_book2_lock.visibility = View.VISIBLE
            activity_main_book2_lock.setOnClickListener {
                (applicationContext as Application).startMusic(R.raw.klik, false)
                purchase(0)
            }
        } else {
            activity_main_book2_lock.visibility = View.GONE
        }
        if (!openingBook3) {
            activity_main_book3_lock.visibility = View.VISIBLE
            activity_main_book3_lock.setOnClickListener {
                (applicationContext as Application).startMusic(R.raw.klik, false)
                purchase(1)
            }
        } else {
            activity_main_book3_lock.visibility = View.GONE
        }
        if (!openingBook4) {
            activity_main_book4_lock.visibility = View.VISIBLE
            activity_main_book4_lock.setOnClickListener {
                (applicationContext as Application).startMusic(R.raw.klik, false)
                purchase(3)
            }
        } else {
            activity_main_book4_lock.visibility = View.GONE
        }
        if (!openingBook5) {
            activity_main_book5_lock.visibility = View.VISIBLE
            activity_main_book5_lock.setOnClickListener {
                (applicationContext as Application).startMusic(R.raw.klik, false)
                purchase(4)
            }
        } else {
            activity_main_book5_lock.visibility = View.GONE
        }

        buy2!!.setOnClickListener {
            (applicationContext as Application).startMusic(R.raw.klik, false)
            purchase(0)
        }
        buy3!!.setOnClickListener {
            (applicationContext as Application).startMusic(R.raw.klik, false)
            purchase(1)
        }
        buy4!!.setOnClickListener {
            (applicationContext as Application).startMusic(R.raw.klik, false)
            purchase(3)
        }
        buy5!!.setOnClickListener {
            (applicationContext as Application).startMusic(R.raw.klik, false)
            purchase(4)
        }
    }

    private fun sendEmail() {
        val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "questgamebook@gmail.com", null))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "")
        if (language == "ru") {
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Привет! Меня зовут ______. Хочу поулучать новости Книги Игр!")
            startActivity(Intent.createChooser(emailIntent, "Отправить письмо"))
        } else {
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Hi! My name is _______. I want to receive Gamebook newsletter!")
            startActivity(Intent.createChooser(emailIntent, "Send message"))
        }

    }


    private fun setLocal(lang: String) {
        val locale = Locale(lang)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        baseContext.resources.updateConfiguration(config, baseContext.resources.displayMetrics)
        getSharedPreferences("Settings", Context.MODE_PRIVATE).edit().putString("lang", lang).apply()

    }

    private fun loadLocal() {
        language = Locale.getDefault().language
        Log.i("LANGUAGE", language)
        setLocal(language)
    }

    private fun openBook4() {
        if (openingBook4) {
            if (language == "ru")
                changeActivity("Переводчик", R.raw.perevodchik_rus, R.drawable.perevodchik_back, R.raw.perevodchik_sheming_weasel, R.drawable.s_perevodchik_cover_rus, "Я пью когда у меня есть повод, и иногда когда у меня нет повода.", "Дон Кихот")
            else
                changeActivity("Interpreter", R.raw.perevodchik_eng, R.drawable.perevodchik_back, R.raw.perevodchik_sheming_weasel, R.drawable.s_perevodchik_cover_eng, "I drink when I have occasion, and sometimes when I have no occasion", "Don Quixote")

        } else {
            buy4_2.setOnClickListener {
                (applicationContext as Application).startMusic(R.raw.klik, false)
                superDialogsShow()
            }
            buy4.setOnClickListener {
                (applicationContext as Application).startMusic(R.raw.klik, false)
                purchase(3)
            }
            buy4.visibility = View.VISIBLE
            buy4.alpha = 0f
            buy4_2.visibility = View.VISIBLE
            buy4_2.alpha = 0f
            buy4.animate()
                    .alpha(1f)
                    .setStartDelay(100)
                    .setDuration(1000)
                    .setListener(object : Animator.AnimatorListener {
                        override fun onAnimationStart(animation: Animator) {}
                        override fun onAnimationEnd(animation: Animator) {
                            buy4!!.animate()
                                    .alpha(0f)
                                    .setStartDelay(2000)
                                    .setDuration(200)
                                    .setListener(object : Animator.AnimatorListener {
                                        override fun onAnimationStart(animation: Animator) {}
                                        override fun onAnimationEnd(animation: Animator) {
                                            buy4.alpha = 0f
                                            buy4.visibility = View.GONE
                                            buy4_2.alpha = 0f
                                            buy4_2.visibility = View.GONE
                                        }

                                        override fun onAnimationCancel(animation: Animator) {}
                                        override fun onAnimationRepeat(animation: Animator) {}
                                    })
                                    .start()
                            buy4_2.animate()
                                    .alpha(0f)
                                    .setStartDelay(2000)
                                    .setDuration(200)
                                    .start()
                        }

                        override fun onAnimationCancel(animation: Animator) {}
                        override fun onAnimationRepeat(animation: Animator) {}
                    })
                    .start()
            buy4_2.animate()
                    .alpha(1f)
                    .setStartDelay(100)
                    .setDuration(1000)
                    .start()
        }
    }

    private fun showCustomDialog(text: String, okText: String, cancelText: String, callback: (Boolean) -> Unit) {
        val builder = AlertDialog.Builder(this)
        val dialogView: View = if (cancelText.isEmpty()) {
            View.inflate(this, R.layout.custom_dialog_free, null)
        } else {
            View.inflate(this, R.layout.custom_dialog, null)
        }

        builder.setView(dialogView)
                .setView(dialogView)
                .setCancelable(true)
        val alert = builder.create()
        alert.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialogView.findViewById<TextView>(R.id.text_custom_dialog).text = text
        val okButton = dialogView.findViewById<Button>(R.id.buy_button)
        okButton.text = okText
        okButton.setOnClickListener {
            (applicationContext as Application).startMusic(R.raw.klik, false)
            callback(true)
            alert.cancel()
        }
        if (cancelText.isNotEmpty()) {
            val cancelButton = dialogView.findViewById<Button>(R.id.cancel_button)
            cancelButton.text = cancelText
            cancelButton.setOnClickListener {
                (applicationContext as Application).startMusic(R.raw.klik, false)
                callback(false)
                alert.cancel()
            }
        }
        alert.show()
    }

    private fun superDialogsShow() {
        val textForSuperDialog = resources.getStringArray(R.array.mark_for_super_dialog)
        var okText = "Далее"
        var yes = "Да"
        var no = "Нет"
        var good = "Хорошо"
        if (language == "en") {
            okText = "Next"
            yes = "Yes"
            no = "No"
            good = "Good"
        }

        val cancelText = ""
        showCustomDialog(textForSuperDialog[0], okText, cancelText) {
            showCustomDialog(textForSuperDialog[1], okText, cancelText) {
                showCustomDialog(textForSuperDialog[2], okText, cancelText) {
                    showCustomDialog(textForSuperDialog[3], okText, cancelText) {
                        showCustomDialog(textForSuperDialog[4], okText, cancelText) {
                            showCustomDialog(textForSuperDialog[5], okText, cancelText) {
                                showCustomDialog(textForSuperDialog[6], okText, cancelText) {
                                    showCustomDialog(textForSuperDialog[7], okText, cancelText) {
                                        showCustomDialog(textForSuperDialog[8], okText, cancelText) {
                                            showCustomDialog(textForSuperDialog[9], yes, no) {
                                                if (!it) showCustomDialog(textForSuperDialog[10], good, cancelText) {}
                                                else showTimerDialog()
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private fun showTimerDialog() {
        val builder = AlertDialog.Builder(this)
        val dialogView = View.inflate(this, R.layout.custom_dialog_timer, null)
        builder.setView(dialogView)
                .setView(dialogView)
                .setCancelable(true)
                .setOnCancelListener {
                    clearTimer()
                }
        val alert = builder.create()
        check1 = dialogView.findViewById(R.id.check1)
        check2 = dialogView.findViewById(R.id.check2)
        check3 = dialogView.findViewById(R.id.check3)
        check22 = dialogView.findViewById(R.id.check2_2)
        timerText = dialogView.findViewById(R.id.timer_text)
        val timerText1 = dialogView.findViewById<TextView>(R.id.text_custom_dialog_text1)
        val timerText2 = dialogView.findViewById<TextView>(R.id.text_custom_dialog_text2)
        val timerText3 = dialogView.findViewById<TextView>(R.id.text_custom_dialog_text3)

        var startTimer = "Start timer"
        var pauseText = "Pause"
        if (language == "ru") {
            startTimer = "Запустить таймер"
            pauseText = "Пауза"
        }
        okButton = dialogView.findViewById(R.id.timer_button)

        okButton.setOnClickListener {
            (applicationContext as Application).startMusic(R.raw.klik, false)
            when (stateTimer) {
                StateTimer.COMPLETE -> {
                    alert.cancel()
                    openBookFree4()
                }
                StateTimer.RUNING -> {
                    stateTimer = StateTimer.PAUSE
                    pauseTimer()
                    okButton.text = startTimer
                    val pointText1 = dialogView.findViewById<ConstraintLayout>(R.id.point_custom_dialog_text1)
                    pointText1.setOnClickListener {}
                    val pointText2 = dialogView.findViewById<ConstraintLayout>(R.id.point_custom_dialog_text2)
                    pointText2.setOnClickListener {}
                    val pointText3 = dialogView.findViewById<ConstraintLayout>(R.id.point_custom_dialog_text3)
                    pointText3.setOnClickListener {}
                }
                StateTimer.PAUSE -> {
                    stateTimer = StateTimer.RUNING
                    restoreTimer()
                    okButton.text = pauseText

                    val pointText1 = dialogView.findViewById<ConstraintLayout>(R.id.point_custom_dialog_text1)
                    pointText1.setOnClickListener {
                        (applicationContext as Application).startMusic(R.raw.klik, false)
                        this.sharedIntentShow()
                        pointText1.setOnClickListener {}
                        check1.visibility = View.VISIBLE
                        check1.setAnimation(R.raw.check)
                        check1.playAnimation()
                        check1.loop(false)
                        if (check1.visibility == View.VISIBLE && check2.visibility == View.VISIBLE && check3.visibility == View.VISIBLE) {
                            endTimer()
                        }
                    }
                    val pointText2 = dialogView.findViewById<ConstraintLayout>(R.id.point_custom_dialog_text2)
                    pointText2.setOnClickListener {
                        (applicationContext as Application).startMusic(R.raw.klik, false)
                        this.sharedIntentShow()
                        sharedCount++
                        check22.text = "$sharedCount/3"
                        if (sharedCount == 3) {
                            pointText2.setOnClickListener {}
                            check2.visibility = View.VISIBLE
                            check22.visibility = View.GONE
                            check2.setAnimation(R.raw.check)
                            check2.playAnimation()
                            check2.loop(false)
                        }
                        if (check1.visibility == View.VISIBLE && check2.visibility == View.VISIBLE && check3.visibility == View.VISIBLE) {
                            endTimer()
                        }
                    }
                    val pointText3 = dialogView.findViewById<ConstraintLayout>(R.id.point_custom_dialog_text3)
                    pointText3.setOnClickListener {
                        (applicationContext as Application).startMusic(R.raw.klik, false)
                        this.sendEmail()
                        pointText3.setOnClickListener {}
                        check3.visibility = View.VISIBLE
                        check3.setAnimation(R.raw.check)
                        check3.playAnimation()
                        check3.loop(false)
                        if (check1.visibility == View.VISIBLE && check2.visibility == View.VISIBLE && check3.visibility == View.VISIBLE) {
                            endTimer()
                        }
                    }

                }
                StateTimer.DONT_START -> {
                    stateTimer = StateTimer.RUNING
                    startTimer()
                    okButton.text = pauseText
                    timerText1.setBackgroundResource(R.drawable.border_button_dialog_timer)
                    timerText2.setBackgroundResource(R.drawable.border_button_dialog_timer)
                    timerText3.setBackgroundResource(R.drawable.border_button_dialog_timer)

                    val pointText1 = dialogView.findViewById<ConstraintLayout>(R.id.point_custom_dialog_text1)
                    pointText1.setOnClickListener {
                        (applicationContext as Application).startMusic(R.raw.klik, false)
                        this.sharedIntentShow()
                        pointText1.setOnClickListener {}
                        check1.visibility = View.VISIBLE
                        check1.setAnimation(R.raw.check)
                        check1.playAnimation()
                        check1.loop(false)
                        if (check1.visibility == View.VISIBLE && check2.visibility == View.VISIBLE && check3.visibility == View.VISIBLE) {
                            endTimer()
                        }
                    }
                    val pointText2 = dialogView.findViewById<ConstraintLayout>(R.id.point_custom_dialog_text2)
                    pointText2.setOnClickListener {
                        (applicationContext as Application).startMusic(R.raw.klik, false)
                        this.sharedIntentShow()
                        sharedCount++
                        check22.text = "$sharedCount/3"
                        if (sharedCount == 3) {
                            pointText2.setOnClickListener {}
                            check2.visibility = View.VISIBLE
                            check22.visibility = View.GONE
                            check2.setAnimation(R.raw.check)
                            check2.playAnimation()
                            check2.loop(false)
                        }
                        if (check1.visibility == View.VISIBLE && check2.visibility == View.VISIBLE && check3.visibility == View.VISIBLE) {
                            endTimer()
                        }
                    }
                    val pointText3 = dialogView.findViewById<ConstraintLayout>(R.id.point_custom_dialog_text3)
                    pointText3.setOnClickListener {
                        (applicationContext as Application).startMusic(R.raw.klik, false)
                        this.sendEmail()
                        pointText3.setOnClickListener {}
                        check3.visibility = View.VISIBLE
                        check3.setAnimation(R.raw.check)
                        check3.playAnimation()
                        check3.loop(false)
                        if (check1.visibility == View.VISIBLE && check2.visibility == View.VISIBLE && check3.visibility == View.VISIBLE) {
                            endTimer()
                        }
                    }
                }
            }
            if (check1.visibility == View.VISIBLE && check2.visibility == View.VISIBLE && check3.visibility == View.VISIBLE) {
                endTimer()
            }
        }
        alert.show()
    }

    enum class StateTimer {
        RUNING,
        PAUSE,
        COMPLETE,
        DONT_START
    }

    private fun openBookFree4() {
        openingBook4 = true
        activity_main_book4_lock.visibility = View.GONE
        getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE).edit().putBoolean(SHARED_BOOK3, true).apply()
        refreshAfterBuying()
    }

    private fun pauseTimer() {
        timerTask.cancel()
        scheduledExecutorServiceTimer.shutdownNow()
    }

    private fun restoreTimer() {
        timerTask = TimerDialogTask()
        scheduledExecutorServiceTimer = Executors.newSingleThreadScheduledExecutor()
        scheduledExecutorServiceTimer.schedule(timerTask, 1, TimeUnit.SECONDS)
    }

    private fun clearTimer() {
        stateTimer = StateTimer.DONT_START
        timer = 0
        timerTask.cancel()
        scheduledExecutorServiceTimer.shutdownNow()
    }

    private fun startTimer() {
        scheduledExecutorServiceTimer = Executors.newSingleThreadScheduledExecutor()
        scheduledExecutorServiceTimer.schedule(timerTask, 1, TimeUnit.SECONDS)
    }

    inner class TimerDialogTask : TimerTask() {
        override fun run() {
            if (timer >= 2 * 60 + 54) {
                endTimer()
            } else {
                if (!scheduledExecutorServiceTimer.isShutdown) {
                    scheduledExecutorServiceTimer.schedule(timerTask, 1, TimeUnit.SECONDS)
                    timer++
                    val time = 2 * 60 + 55 - timer
                    if (time > 0) {
                        Log.i("TIMER_TASK_DEBUG_TEST", "if2")
                        val minutes = (time / 60)
                        val second = time - minutes * 60
                        val digit1 = minutes / 10
                        val digit2 = minutes % 10
                        val digit3 = second / 10
                        val digit4 = second % 10
                        runOnUiThread {
                            timerText.text = "$digit1$digit2:$digit3$digit4"
                        }
                    }
                }

            }
        }
    }

    private fun endTimer() {
        try {
            stateTimer = StateTimer.COMPLETE
            runOnUiThread {
                if (language == "ru")
                    okButton.text = "Дело в шляпе"
                else
                    okButton.text = "All done!"
            }
            scheduledExecutorServiceTimer.shutdownNow()
        } catch (e: Exception) {
        }
    }

    private fun openBook5() {
        if (openingBook5) {
            if (language == "ru")
                changeActivity("День из практики", R.raw.den_rus, R.drawable.praktika_back, R.raw.den_jazz_comedy, R.drawable.s_praktika_cover_rus, "Morbi perniciores pluresque animi quam corporis.", "Цицерон")
            else
                changeActivity("Internship", R.raw.den_eng, R.drawable.praktika_back, R.raw.den_jazz_comedy, R.drawable.s_praktika_cover_eng, "Morbi perniciores pluresque animi quam corporis.", "Cicero")

        } else {
            buy5.setOnClickListener {
                (applicationContext as Application).startMusic(R.raw.klik, false)
                purchase(4)
            }
            buy5.visibility = View.VISIBLE
            buy5.alpha = 0f
            buy5.animate()
                    .alpha(1f)
                    .setStartDelay(100)
                    .setDuration(1000)
                    .setListener(object : Animator.AnimatorListener {
                        override fun onAnimationStart(animation: Animator) {}
                        override fun onAnimationEnd(animation: Animator) {
                            buy5.animate()
                                    .alpha(0f)
                                    .setStartDelay(2000)
                                    .setDuration(200)
                                    .setListener(object : Animator.AnimatorListener {
                                        override fun onAnimationStart(animation: Animator) {}
                                        override fun onAnimationEnd(animation: Animator) {
                                            buy5.alpha = 0f
                                            buy5.visibility = View.GONE
                                        }

                                        override fun onAnimationCancel(animation: Animator) {}
                                        override fun onAnimationRepeat(animation: Animator) {}
                                    })
                                    .start()
                        }

                        override fun onAnimationCancel(animation: Animator) {}
                        override fun onAnimationRepeat(animation: Animator) {}
                    })
                    .start()
        }
    }

    private fun purchase(number: Int) {
        try {
            purchaseProduct(purchases[number])
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun communityOpenLink(v: View) {
        val valueAnimator: ValueAnimator
        val start: Float
        val end: Float
        if (!resources.getBoolean(R.bool.isTablet)) {
            start = SMART_START_FONT_SIZE
            end = SMART_END_FONT_SIZE
        } else {
            start = TABLET_START_FONT_SIZE
            end = TABLET_END_FONT_SIZE
        }
        valueAnimator = ValueAnimator.ofFloat(
                start,
                end)
        valueAnimator.duration = 100
        valueAnimator.repeatCount = 1
        valueAnimator.repeatMode = ValueAnimator.REVERSE
        valueAnimator.addUpdateListener { animation -> (v as TextView).textSize = animation.animatedValue as Float }
        valueAnimator.start()

        val intent = Intent(Intent.ACTION_VIEW)
        intent.data = Uri.parse("https://romanvartanov.wixsite.com/knigaigr")
        intent.action = Intent.ACTION_VIEW
        startActivity(intent)
    }

    private fun markedAppInGooglePlay(v: View) {
        val valueAnimator: ValueAnimator
        val start: Float
        val end: Float
        if (!resources.getBoolean(R.bool.isTablet)) {
            start = SMART_START_FONT_SIZE
            end = SMART_END_FONT_SIZE
        } else {
            start = TABLET_START_FONT_SIZE
            end = TABLET_END_FONT_SIZE
        }
        valueAnimator = ValueAnimator.ofFloat(
                start,
                end)
        valueAnimator.duration = 100
        valueAnimator.repeatCount = 1
        valueAnimator.repeatMode = ValueAnimator.REVERSE
        valueAnimator.addUpdateListener { animation -> (v as TextView).textSize = animation.animatedValue as Float }
        valueAnimator.start()
        val mark: String = if (language == "ru") {
            "Вам нравится книга игр?"
        } else {
            "Do you like Gamebook?"
        }
        val yes: String = if (language == "ru") {
            "Да"
        } else {
            "Yes"
        }
        val no: String = if (language == "ru") {
            "Нет"
        } else {
            "No"
        }
        AlertDialog.Builder(this@MainActivity).setMessage(mark)
                .setPositiveButton(yes) { dialog, _ ->
                    (applicationContext as Application).startMusic(R.raw.klik, false)
                    val appPackageName = packageName
                    try {
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
                    } catch (anfe: ActivityNotFoundException) {
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
                    } finally {
                        dialog.dismiss()
                    }
                }
                .setNegativeButton(no) { dialog, _ ->
                    (applicationContext as Application).startMusic(R.raw.klik, false)
                    dialog.dismiss()
                    showSendEmailMarkDialog()
                }
                .show()
    }

    private fun showSendEmailMarkDialog() {
        val mark: String = if (language == "ru") {
            "Пожалуйста, напишите нам ваши пожелания на почту"
        } else {
            "Please, let us know your opinion."
        }
        val yes: String = if (language == "ru") {
            "Да"
        } else {
            "Yes"
        }
        val no: String = if (language == "ru") {
            "Нет"
        } else {
            "No"
        }
        AlertDialog.Builder(this@MainActivity).setMessage(mark)
                .setPositiveButton(yes) { dialog, _ ->
                    (applicationContext as Application).startMusic(R.raw.klik, false)
                    showSendEmailMark()
                }
                .setNegativeButton(no) { dialog, _ ->
                    (applicationContext as Application).startMusic(R.raw.klik, false)
                    dialog.dismiss()
                }
                .show()
    }

    private fun showSendEmailMark() {
        val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "questgamebook@gmail.com", null))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "")
        if (language == "ru") {
            emailIntent.putExtra(Intent.EXTRA_TEXT, "")
            startActivity(Intent.createChooser(emailIntent, "Отправить письмо"))
        } else {
            emailIntent.putExtra(Intent.EXTRA_TEXT, "")
            startActivity(Intent.createChooser(emailIntent, "Send message"))
        }
    }

    private fun sharedFunction(v: View) {
        val valueAnimator: ValueAnimator
        val start: Float
        val end: Float
        if (!resources.getBoolean(R.bool.isTablet)) {
            start = SMART_START_FONT_SIZE
            end = SMART_END_FONT_SIZE
        } else {
            start = TABLET_START_FONT_SIZE
            end = TABLET_END_FONT_SIZE
        }
        valueAnimator = ValueAnimator.ofFloat(
                start,
                end)
        valueAnimator.duration = 100
        valueAnimator.repeatCount = 1
        valueAnimator.repeatMode = ValueAnimator.REVERSE
        valueAnimator.addUpdateListener { animation -> (v as TextView).textSize = animation.animatedValue as Float }
        valueAnimator.start()
        sharedIntentShow()

    }

    private fun sharedIntentShow() {
        if (language == "ru") {
            try {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.type = "text/plain"
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Книга Игр")
                val shareIntent1 = Intent(Intent.ACTION_SEND)
                shareIntent1.type = "text/plain"
                shareIntent1.putExtra(Intent.EXTRA_TEXT, "Книга Игр")
                shareIntent1.setPackage("com.instagram.android")
                val shareIntent2 = Intent(Intent.ACTION_SEND)
                shareIntent2.type = "text/plain"
                shareIntent2.putExtra(Intent.EXTRA_TEXT, "Книга Игр")
                shareIntent2.setPackage("com.facebook.katana")
                var shareMessage = "\nДорогой друг, я пишу тебе, чтобы сообщить преприятнейшее известие: Книга Игр теперь доступа в Play Store! \n\n"
                shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n"
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                val chooserIntent = Intent.createChooser(shareIntent, "Select Image")
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(shareIntent1, shareIntent2))
                startActivity(chooserIntent)
            } catch (ignored: Exception) {
            }
        } else {
            try {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.type = "text/plain"
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Bookgame")
                val shareIntent1 = Intent(Intent.ACTION_SEND)
                shareIntent1.type = "text/plain"
                shareIntent1.putExtra(Intent.EXTRA_TEXT, "Bookgame")
                shareIntent1.setPackage("com.instagram.android")
                val shareIntent2 = Intent(Intent.ACTION_SEND)
                shareIntent2.type = "text/plain"
                shareIntent2.putExtra(Intent.EXTRA_TEXT, "Bookgame")
                shareIntent2.setPackage("com.facebook.katana")
                var shareMessage = "\nDear friend, I am writing to inform you of a pleasant piece of news: Gamebook is now available in Play Store.\n\n"
                shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n"
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                val chooserIntent = Intent.createChooser(shareIntent, "Select Image")
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, arrayOf(shareIntent1, shareIntent2))
                startActivity(chooserIntent)
            } catch (ignored: Exception) {
            }
        }
    }

    private fun openBook3() {
        if (openingBook3) {
            if (language == "ru")
                changeActivity("Еще один вторник", R.raw.vtornik_rus, R.drawable.vtornik_back, R.raw.fon_vtornik, R.drawable.s_vtornik_cover_rus, "Fastidium est quies", "")
            else
                changeActivity("Yet Another Tuesday", R.raw.vtornik_eng, R.drawable.vtornik_back, R.raw.fon_vtornik, R.drawable.s_vtornik_cover_eng, "Fastidium est quies", "")
        } else {
            buy3.setOnClickListener {
                (applicationContext as Application).startMusic(R.raw.klik, false)
                purchase(1)
            }
            buy3.visibility = View.VISIBLE
            buy3.alpha = 0f
            buy3.animate()
                    .alpha(1f)
                    .setStartDelay(100)
                    .setDuration(1000)
                    .setListener(object : Animator.AnimatorListener {
                        override fun onAnimationStart(animation: Animator) {}

                        override fun onAnimationEnd(animation: Animator) {
                            buy3.animate()
                                    .alpha(0f)
                                    .setStartDelay(2000)
                                    .setDuration(200)
                                    .setListener(object : Animator.AnimatorListener {
                                        override fun onAnimationStart(animation: Animator) {}

                                        override fun onAnimationEnd(animation: Animator) {
                                            buy3.alpha = 0f
                                            buy3.visibility = View.GONE
                                        }

                                        override fun onAnimationCancel(animation: Animator) {

                                        }

                                        override fun onAnimationRepeat(animation: Animator) {

                                        }
                                    })
                                    .start()
                        }

                        override fun onAnimationCancel(animation: Animator) {

                        }

                        override fun onAnimationRepeat(animation: Animator) {

                        }
                    })
                    .start()

        }
    }

    private fun showDialogLike() {
        val mark: String = if (language == "ru") {
            "Вам нравится книга игр?"
        } else {
            "Do you like Gamebook?"
        }
        val yes: String = if (language == "ru") {
            "Да"
        } else {
            "Yes"
        }
        val no: String = if (language == "ru") {
            "Нет"
        } else {
            "No"
        }
        AlertDialog.Builder(this).setMessage(mark)
                .setPositiveButton(yes) { dialog, _ ->
                    (applicationContext as Application).startMusic(R.raw.klik, false)
                    val appPackageName = packageName
                    try {
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
                    } catch (anfe: ActivityNotFoundException) {
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
                    } finally {
                        dialog.dismiss()
                        openingBook2 = true
                        getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE).edit().putBoolean(SHARED_BOOK1, true).apply()
                        refreshAfterBuying()
                    }
                }
                .setNegativeButton(no) { dialog, _ ->
                    (applicationContext as Application).startMusic(R.raw.klik, false)
                    dialog.cancel()
                }
                .show()
    }

    private fun openBook2() {
        if (openingBook2)
            if (language == "ru")
                changeActivity("Суд присяжных", R.raw.sud_rus, R.drawable.sud_back, R.raw.fon_suda, R.drawable.s_sud_cover_rus, "Все животные равны, но некоторые животные равнее других", "Джордж Оруэлл")
            else
                changeActivity("Jury Duty", R.raw.sud_eng, R.drawable.sud_back, R.raw.fon_suda, R.drawable.s_sud_cover_eng, "All animals are equal, but some animals are more equal than others", "George Orwell")
        else {
            buy2_2.setOnClickListener {
                (applicationContext as Application).startMusic(R.raw.klik, false)
                showDialogLike()
            }
            buy2.setOnClickListener {
                (applicationContext as Application).startMusic(R.raw.klik, false)
                purchase(0)
            }
            buy2.visibility = View.VISIBLE
            buy2.alpha = 0f
            buy2_2.visibility = View.VISIBLE
            buy2_2.alpha = 0f
            buy2.animate()
                    .alpha(1f)
                    .setStartDelay(100)
                    .setDuration(1000)
                    .setListener(object : Animator.AnimatorListener {
                        override fun onAnimationStart(animation: Animator) {}

                        override fun onAnimationEnd(animation: Animator) {
                            buy2.animate()
                                    .alpha(0f)
                                    .setStartDelay(2000)
                                    .setDuration(200)
                                    .setListener(object : Animator.AnimatorListener {
                                        override fun onAnimationStart(animation: Animator) {}

                                        override fun onAnimationEnd(animation: Animator) {
                                            buy2.alpha = 0f
                                            buy2.visibility = View.GONE
                                            buy2_2.alpha = 0f
                                            buy2_2.visibility = View.GONE
                                        }

                                        override fun onAnimationCancel(animation: Animator) {

                                        }

                                        override fun onAnimationRepeat(animation: Animator) {

                                        }
                                    })
                                    .start()
                            buy2_2.animate()
                                    .alpha(0f)
                                    .setStartDelay(2000)
                                    .setDuration(200)
                                    .start()

                        }

                        override fun onAnimationCancel(animation: Animator) {

                        }

                        override fun onAnimationRepeat(animation: Animator) {

                        }
                    })
                    .start()
            buy2_2.animate()
                    .alpha(1f)
                    .setStartDelay(100)
                    .setDuration(1000)
                    .start()

        }
    }


    @SuppressLint("StaticFieldLeak")
    private fun hidePopView() {
        object : AsyncTask<Void?, Void?, Void?>() {

            private var intro: TextView? = null
            private var quoteView: TextView? = null
            private var authorQuoteView: TextView? = null
            private var menu: ScrollView? = null
            private var citats: Array<String>? = null
            private var authors: Array<String>? = null
            private var quote: String? = null
            private var authorQuote: String? = null

            override fun doInBackground(vararg voids: Void?): Void? {
                try {
                    quoteView = findViewById(R.id.activity_main_quote)
                    authorQuoteView = findViewById(R.id.activity_main_author_quote)
                    intro = findViewById(R.id.activity_main_intro)
                    menu = findViewById(R.id.activity_main_container_menu)
                    citats = resources.getStringArray(R.array.citats)
                    authors = resources.getStringArray(R.array.authors_citats)
                    val num = Random().nextInt(citats!!.size)
                    quote = citats!![num]
                    authorQuote = authors!![num]
                    if (intent.getBooleanExtra("firstStart", true))
                        TimeUnit.SECONDS.sleep(2)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

                return null
            }

            override fun onPostExecute(aVoid: Void?) {

                menu!!.alpha = 0f
                menu!!.visibility = View.VISIBLE
                menu!!.animate().alpha(1f).duration = 1000
                quoteView!!.text = quote
                authorQuoteView!!.text = authorQuote
                intro!!.visibility = View.GONE
                super.onPostExecute(aVoid)
            }
        }.execute()
    }

    @SuppressLint("StaticFieldLeak")
    private fun updateImages() {
        // Custom Splash
        if (!intent.getBooleanExtra("firstStart", true)) {
            findViewById<View>(R.id.activity_main_intro).visibility = View.GONE
        }
        // change SplashScreen on drawable
        val background = findViewById<LinearLayout>(R.id.activity_main_main)
        background.background = BitmapDrawable(
                app!!.decodeSampledBitmapFromResource(resources, R.drawable.activity_main_back, 200, 400))

        object : AsyncTask<Void?, Void?, Void?>() {

            lateinit var bitBook6: Bitmap
            lateinit var bitBook1: Bitmap
            lateinit var bitBook2: Bitmap
            lateinit var bitBook3: Bitmap
            lateinit var bitBook4: Bitmap
            lateinit var bitBook5: Bitmap


            override fun doInBackground(vararg voids: Void?): Void? {
                if (language == "ru") {
                    bitBook1 = app!!.decodeSampledBitmapFromResource(resources, R.drawable.buro_cover_rus,
                            app!!.pxFromDp(WIDTH_IMAGE_DP.toFloat()).toInt(), app!!.pxFromDp(HEIGHT_IMAGE_DP.toFloat()).toInt())

                    bitBook2 = app!!.decodeSampledBitmapFromResource(resources, R.drawable.sud_cover_rus,
                            app!!.pxFromDp(WIDTH_IMAGE_DP.toFloat()).toInt(), app!!.pxFromDp(HEIGHT_IMAGE_DP.toFloat()).toInt())

                    bitBook3 = app!!.decodeSampledBitmapFromResource(resources, R.drawable.vtornik_cover_rus,
                            app!!.pxFromDp(WIDTH_IMAGE_DP.toFloat()).toInt(), app!!.pxFromDp(HEIGHT_IMAGE_DP.toFloat()).toInt())

                    bitBook4 = app!!.decodeSampledBitmapFromResource(resources, R.drawable.perevodchik_cover_rus,
                            app!!.pxFromDp(WIDTH_IMAGE_DP.toFloat()).toInt(), app!!.pxFromDp(HEIGHT_IMAGE_DP.toFloat()).toInt())

                    bitBook5 = app!!.decodeSampledBitmapFromResource(resources, R.drawable.practika_cover_rus,
                            app!!.pxFromDp(WIDTH_IMAGE_DP.toFloat()).toInt(), app!!.pxFromDp(HEIGHT_IMAGE_DP.toFloat()).toInt())
                    bitBook6 = app!!.decodeSampledBitmapFromResource(resources, R.drawable.stub_rus,
                            app!!.pxFromDp(WIDTH_IMAGE_DP.toFloat()).toInt(), app!!.pxFromDp(HEIGHT_IMAGE_DP.toFloat()).toInt())
                } else {
                    bitBook1 = app!!.decodeSampledBitmapFromResource(resources, R.drawable.buro_cover_eng,
                            app!!.pxFromDp(WIDTH_IMAGE_DP.toFloat()).toInt(), app!!.pxFromDp(HEIGHT_IMAGE_DP.toFloat()).toInt())

                    bitBook2 = app!!.decodeSampledBitmapFromResource(resources, R.drawable.sud_cover_eng,
                            app!!.pxFromDp(WIDTH_IMAGE_DP.toFloat()).toInt(), app!!.pxFromDp(HEIGHT_IMAGE_DP.toFloat()).toInt())

                    bitBook3 = app!!.decodeSampledBitmapFromResource(resources, R.drawable.vtornik_cover_eng,
                            app!!.pxFromDp(WIDTH_IMAGE_DP.toFloat()).toInt(), app!!.pxFromDp(HEIGHT_IMAGE_DP.toFloat()).toInt())

                    bitBook4 = app!!.decodeSampledBitmapFromResource(resources, R.drawable.perevodchik_cover_eng,
                            app!!.pxFromDp(WIDTH_IMAGE_DP.toFloat()).toInt(), app!!.pxFromDp(HEIGHT_IMAGE_DP.toFloat()).toInt())

                    bitBook5 = app!!.decodeSampledBitmapFromResource(resources, R.drawable.practika_cover_eng,
                            app!!.pxFromDp(WIDTH_IMAGE_DP.toFloat()).toInt(), app!!.pxFromDp(HEIGHT_IMAGE_DP.toFloat()).toInt())
                    bitBook6 = app!!.decodeSampledBitmapFromResource(resources, R.drawable.stub_eng,
                            app!!.pxFromDp(WIDTH_IMAGE_DP.toFloat()).toInt(), app!!.pxFromDp(HEIGHT_IMAGE_DP.toFloat()).toInt())
                }
                return null
            }

            override fun onPostExecute(aVoid: Void?) {
                super.onPostExecute(aVoid)
                activity_main_book1.setImageBitmap(bitBook1)
                activity_main_book2.setImageBitmap(bitBook2)
                activity_main_book3.setImageBitmap(bitBook3)
                activity_main_book4.setImageBitmap(bitBook4)
                activity_main_book5.setImageBitmap(bitBook5)
                activity_main_book6.setImageBitmap(bitBook6)
            }
        }.execute()
    }

    private fun changeActivity(title: String, file: Int?, image: Int?, music: Int?, background: Int?, citat: String, autorCitat: String) {
        val intent = Intent(this@MainActivity, BookActivity::class.java)
        intent.putExtra("title", title)
        intent.putExtra("citat", citat)
        intent.putExtra("autorCitat", autorCitat)
        intent.putExtra("file", file)
        intent.putExtra("image", image)
        intent.putExtra("music", music)
        intent.putExtra("change", changeAnswer)
        intent.putExtra("progress_background", background)
        startActivity(intent)
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }


    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }

    override fun onResume() {
        super.onResume()
        (application as Application).changeThemeMusic(R.raw.fon_main)
    }

    public override fun onPause() {
        (application as Application).stopMusic()
        super.onPause()
    }

    public override fun onDestroy() {
        super.onDestroy()
        if (serviceConnection != null) {
            unbindService(serviceConnection)
        }
    }

    @Throws(Exception::class)
    private fun getInAppPurchases(vararg productIds: String): List<InAppProduct> {
        val skuList = ArrayList(Arrays.asList(*productIds))
        val query = Bundle()
        query.putStringArrayList("ITEM_ID_LIST", skuList)
        val skuDetails = inAppBillingService!!.getSkuDetails(
                3, packageName, "inapp", query)
        val responseList = skuDetails.getStringArrayList("DETAILS_LIST")
        val result = ArrayList<InAppProduct>()
        for (responseItem in responseList!!) {
            val jsonObject = JSONObject(responseItem)
            val product = InAppProduct()
            product.sku = jsonObject.getString("productId")
            product.storeName = jsonObject.getString("title")
            product.storeDescription = jsonObject.getString("description")
            product.price = jsonObject.getString("price")
            product.isSubscription = jsonObject.getString("type") == "subs"
            product.priceAmountMicros = Integer.parseInt(jsonObject.getString("price_amount_micros"))
            product.currencyIsoCode = jsonObject.getString("price_currency_code")
            result.add(product)
        }
        return result
    }


    @Throws(Exception::class)
    private fun purchaseProduct(product: InAppProduct) {
        val sku = product.sku
        val type = product.type
        val buyIntentBundle = inAppBillingService!!.getBuyIntent(
                3, packageName,
                sku, type, null)
        val pendingIntent = buyIntentBundle.getParcelable<PendingIntent>("BUY_INTENT")
        startIntentSenderForResult(pendingIntent!!.intentSender,
                REQUEST_CODE_BUY, Intent(), 0, 0,
                0, null)
    }

    private fun readPurchase(purchaseData: String) {
        try {
            val jsonObject = JSONObject(purchaseData)
            when (jsonObject.getString("productId")) {
                PRODUCT1 -> {
                    openingBook2 = true
                    getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE).edit().putBoolean(SHARED_BOOK1, true).apply()
                    if (language == "ru") {
                        Toast.makeText(this, "Поздравляем!", Toast.LENGTH_SHORT).show()
                        Toast.makeText(this, "Вы открыли Книгу - Суд присяжных", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this, "Congratulation!", Toast.LENGTH_SHORT).show()
                        Toast.makeText(this, "You opened the Book - Jury trial.", Toast.LENGTH_SHORT).show()
                    }
                }
                PRODUCT2 -> {
                    openingBook3 = true
                    getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE).edit().putBoolean(SHARED_BOOK2, true).apply()
                    if (language == "ru") {
                        Toast.makeText(this, "Поздравляем!", Toast.LENGTH_SHORT).show()
                        Toast.makeText(this, "Вы открыли Книгу - Еще один вторник", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this, "Congratulation!", Toast.LENGTH_SHORT).show()
                        Toast.makeText(this, "You opened the Book - Another Tuesday", Toast.LENGTH_SHORT).show()
                    }
                }
                PRODUCT3 -> {
                    Toast.makeText(baseContext, "@@@@@@@@@@@@@@@@@@@@", Toast.LENGTH_SHORT).show()
                    changeAnswer = true
                    getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE).edit().putBoolean(CHANGE_ANSWER, true).apply()
                    if (language == "ru") {
                        Toast.makeText(this, "Поздравляем!", Toast.LENGTH_SHORT).show()
                        Toast.makeText(this, "Вы открыли возможность изменять ответ", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this, "Congratulation!", Toast.LENGTH_SHORT).show()
                        Toast.makeText(this, "You have opened the possibility to change choice", Toast.LENGTH_SHORT).show()
                    }
                }
                PRODUCT4 -> {
                    openingBook4 = true
                    getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE).edit().putBoolean(SHARED_BOOK3, true).apply()
                    if (language == "ru") {
                        Toast.makeText(this, "Поздравляем!", Toast.LENGTH_SHORT).show()
                        Toast.makeText(this, "Вы открыли Книгу - Переводчик", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this, "Congratulation!", Toast.LENGTH_SHORT).show()
                        Toast.makeText(this, "You opened the Book - Interpreter", Toast.LENGTH_SHORT).show()
                    }
                }
                PRODUCT5 -> {
                    openingBook5 = true
                    getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE).edit().putBoolean(SHARED_BOOK4, true).apply()
                    if (language == "ru") {
                        Toast.makeText(this, "Поздравляем!", Toast.LENGTH_SHORT).show()
                        Toast.makeText(this, "Вы открыли Книгу - День из практики", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this, "Congratulation!", Toast.LENGTH_SHORT).show()
                        Toast.makeText(this, "You opened the Book - Internship", Toast.LENGTH_SHORT).show()
                    }
                }
            }
            refreshAfterBuying()

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_BUY) {
            val responseCode = data!!.getIntExtra("RESPONSE_CODE", -1)
            if (responseCode == BILLING_RESPONSE_RESULT_OK) {
                val purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA")
                readPurchase(purchaseData)
            }
        }
    }

    inner class InAppProduct {

        var sku: String? = null
            internal set
        internal var storeName: String? = null
        internal var storeDescription: String? = null
        internal var price: String? = null
        internal var isSubscription: Boolean = false
        internal var priceAmountMicros: Int = 0
        internal var currencyIsoCode: String? = null

        val type: String
            get() = if (isSubscription) "subs" else "inapp"

    }

    companion object {

        /**
         * Constants
         */
        const val REQUEST_CODE_BUY = 1234
        const val BILLING_RESPONSE_RESULT_OK = 0
        const val PURCHASE_STATUS_PURCHASED = 0
        private const val WIDTH_IMAGE_DP = 140
        private const val HEIGHT_IMAGE_DP = 200
        private const val PRODUCT1 = "ru.bookgame.bookgame_testing_inapp1"
        private const val PRODUCT2 = "ru.bookgame.bookgame_testing_inapp2"
        const val PRODUCT3 = "ru.bookgame.bookgame_testing_inapp3"
        const val PRODUCT4 = "ru.bookgame.bookgame_testing_inapp4"
        const val PRODUCT5 = "ru.bookgame.bookgame_testing_inapp5"
        const val SHARED_KEY = "BOOK"
        private const val SHARED_BOOK1 = "book2"
        private const val SHARED_BOOK2 = "book3"
        private const val SHARED_BOOK3 = "book4"
        private const val SHARED_BOOK4 = "book5"
        const val CHANGE_ANSWER = "change_answer"
        const val TABLET_START_FONT_SIZE = 20f
        const val TABLET_END_FONT_SIZE = 22f
        const val SMART_START_FONT_SIZE = 16f
        const val SMART_END_FONT_SIZE = 18f
        var changeAnswer = false
        var purchases = listOf<InAppProduct>()
    }
}
