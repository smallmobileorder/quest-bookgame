package ru.bookgame.bookgame.android.activity

import android.animation.ValueAnimator
import android.animation.ValueAnimator.REVERSE
import android.app.Activity
import android.app.PendingIntent
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.Intent.FLAG_ACTIVITY_CLEAR_TOP
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_settings.*
import org.json.JSONObject
import ru.bookgame.bookgame.BuildConfig
import ru.bookgame.bookgame.R
import ru.bookgame.bookgame.android.Application
import ru.bookgame.bookgame.android.activity.MainActivity.*
import ru.bookgame.bookgame.android.activity.MainActivity.Companion.BILLING_RESPONSE_RESULT_OK
import ru.bookgame.bookgame.android.activity.MainActivity.Companion.CHANGE_ANSWER
import ru.bookgame.bookgame.android.activity.MainActivity.Companion.PRODUCT3
import ru.bookgame.bookgame.android.activity.MainActivity.Companion.SHARED_KEY
import ru.bookgame.bookgame.android.activity.MainActivity.Companion.SMART_END_FONT_SIZE
import ru.bookgame.bookgame.android.activity.MainActivity.Companion.SMART_START_FONT_SIZE
import ru.bookgame.bookgame.android.activity.MainActivity.Companion.TABLET_END_FONT_SIZE
import ru.bookgame.bookgame.android.activity.MainActivity.Companion.TABLET_START_FONT_SIZE
import ru.bookgame.bookgame.android.activity.MainActivity.Companion.changeAnswer
import ru.bookgame.bookgame.android.activity.MainActivity.Companion.purchases
import java.util.*
import ru.bookgame.bookgame.android.activity.MainActivity.Companion.REQUEST_CODE_BUY as REQUEST_CODE_BUY1

class SettingsActivity : AppCompatActivity(), View.OnClickListener {

    lateinit var myApplication: Application
    var isPause: Boolean = true
    var language = "en"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_settings)
        language = Locale.getDefault().language
        val background = findViewById<LinearLayout>(R.id.activity_settings_main)
        myApplication = application as Application
        background.background = BitmapDrawable(
                myApplication.decodeSampledBitmapFromResource(resources, intent.getIntExtra("image", 0), 100, 200)
        )
        val stringBuilder = StringBuilder()
        stringBuilder.append(intent.getIntExtra("openCountEnds", 0))
                .append("/")
                .append(intent.getIntExtra("countEnds", 0))
        findViewById<TextView>(R.id.activity_settings_txt_open_ends).text = stringBuilder.toString()

        initButtons()
    }

    private fun initButtons() {
        val sharedPreferences = getSharedPreferences(getString(R.string.is_play_music), Context.MODE_PRIVATE)!!
        val isPlayMusic = sharedPreferences.getBoolean(getString(R.string.is_play_music), true)
        activity_settings_swtch_music.isChecked = isPlayMusic
        activity_settings_swtch_music.setOnCheckedChangeListener { _, isChecked ->
            sharedPreferences.edit().putBoolean(getString(R.string.is_play_music), isChecked).apply()
            if (!isChecked) {
                myApplication.stopMusic()
            } else {
                myApplication.changeThemeMusic(intent.getIntExtra("music", 1))
            }
        }
        val isPlaySound = sharedPreferences.getBoolean(getString(R.string.is_play_sound), true)
        activity_settings_swtch_sounds.isChecked = isPlaySound
        activity_settings_swtch_sounds.setOnCheckedChangeListener { _, isChecked ->
            sharedPreferences.edit().putBoolean(getString(R.string.is_play_sound), isChecked).apply()
        }
    }

    private fun showSendEmailMarkDialog() {
        val mark: String = if (language == "ru") {
            "Пожалуйста, напишите нам ваши пожелания на почту"
        } else {
            "Please, let us know your opinion."
        }
        val yes: String = if (language == "ru") {
            "Да"
        } else {
            "Yes"
        }
        val no: String = if (language == "ru") {
            "Нет"
        } else {
            "No"
        }
        AlertDialog.Builder(this@SettingsActivity).setMessage(mark)
                .setPositiveButton(yes) { dialog, _ ->
                    (applicationContext as Application).startMusic(R.raw.klik, false)
                    showSendEmailEmpty()
                }
                .setNegativeButton(no) { dialog, _ ->
                    (applicationContext as Application).startMusic(R.raw.klik, false)
                    dialog.dismiss()
                }
                .show()
    }

    private fun showSendEmailEmpty() {
        val emailIntent = Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", "questgamebook@gmail.com", null))
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "")
        if (language == "ru") {
            emailIntent.putExtra(Intent.EXTRA_TEXT, "")
            startActivity(Intent.createChooser(emailIntent, "Отправить письмо"))
        } else {
            emailIntent.putExtra(Intent.EXTRA_TEXT, "")
            startActivity(Intent.createChooser(emailIntent, "Send message"))
        }
    }

    override fun onClick(v: View) {
        animateClick(v)
        (applicationContext as Application).startMusic(R.raw.klik, false)
        when (v.id) {
            R.id.activity_settings_btn_restart -> {
                val startNewGame: String = if (language == "ru") {
                    "Начать новую игру?"
                } else {
                    "Start New Game"
                }
                AlertDialog.Builder(this).setMessage(startNewGame)
                        .setPositiveButton("Да") { _, _ ->
                            (applicationContext as Application).startMusic(R.raw.klik, false)

                            val intent = Intent()
                            intent.putExtra("restart", true)
                            setResult(Activity.RESULT_OK, intent)
                            finish()
                            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                        }
                        .setNegativeButton("Нет") { dialog, _ ->
                            (applicationContext as Application).startMusic(R.raw.klik, false)
                            dialog.cancel()
                        }
                        .show()
            }
            R.id.activity_settings_btn_change -> {
                if (changeAnswer) {
                    val intent = Intent()
                    intent.putExtra("changeBtn", true)
                    setResult(Activity.RESULT_OK, intent)
                    isPause = true
                    finish()
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
                } else {
                    showCustomDialog()
                }
            }
            R.id.activity_settings_btn_community -> {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse("https://romanvartanov.wixsite.com/knigaigr")
                intent.action = Intent.ACTION_VIEW
                startActivity(intent)
            }
            R.id.activity_settings_btn_good_deal -> {
                val mark: String = if (language == "ru") {
                    "Вам нравится книга игр?"
                } else {
                    "Do you like Gamebook?"
                }
                val yes: String = if (language == "ru") {
                    "Да"
                } else {
                    "Yes"
                }
                val no: String = if (language == "ru") {
                    "Нет"
                } else {
                    "No"
                }
                AlertDialog.Builder(this@SettingsActivity).setMessage(mark)
                        .setPositiveButton(yes) { dialog, _ ->
                            (applicationContext as Application).startMusic(R.raw.klik, false)
                            val appPackageName = packageName
                            try {
                                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$appPackageName")))
                            } catch (anfe: ActivityNotFoundException) {
                                startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")))
                            } finally {
                                dialog.dismiss()
                            }
                        }
                        .setNegativeButton(no) { dialog, _ ->
                            (applicationContext as Application).startMusic(R.raw.klik, false)
                            dialog.dismiss()
                            showSendEmailMarkDialog()
                        }
                        .show()
            }
            R.id.activity_settings_btn_endings -> {
                val intent = Intent(this, EndsActivity::class.java)
                intent.putExtras(getIntent())
                isPause = true
                startActivity(intent)
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
            }
            R.id.activity_settings_btn_menu -> {
                val intent = Intent(this, MainActivity::class.java)
                intent.flags = FLAG_ACTIVITY_CLEAR_TOP
                intent.putExtra("firstStart", false)
                startActivity(intent)
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)

            }
            R.id.activity_settings_btn_recover -> {
                val success: String = if (language == "ru") {
                    "Успешно"
                } else {
                    "Success"
                }
                val purchase: String = if (language == "ru") {
                    "Ваши покупки были восстановлены"
                } else {
                    "Your purchases have been restored"
                }
                val no: String = if (language == "ru") {
                    "Отмена"
                } else {
                    "Cancel"
                }
                val ok: String = if (language == "ru") {
                    "Ок"
                } else {
                    "Ok"
                }
                AlertDialog.Builder(this)
                        .setTitle(success)
                        .setMessage(purchase)
                        .setPositiveButton(ok) { dialog, _ ->
                            (applicationContext as Application).startMusic(R.raw.klik, false)
                            dialog.cancel()
                        }
                        .setNegativeButton(no) { dialog, _ ->
                            (applicationContext as Application).startMusic(R.raw.klik, false)
                            dialog.cancel()
                        }
                        .show()
            }
            R.id.activity_settings_btn_share -> {
                if (language == "ru") {
                    try {
                        val shareIntent = Intent(Intent.ACTION_SEND)
                        shareIntent.type = "text/plain"
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Книга Игр")
                        var shareMessage = "\nДорогой друг, я пишу тебе, чтобы сообщить преприятнейшее известие: Книга Игр теперь доступа в Play Store!\n\n"
                        shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n"
                        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                        startActivity(Intent.createChooser(shareIntent, "choose one"))
                    } catch (ignored: Exception) {
                        ignored.printStackTrace()
                    }
                } else {
                    try {
                        val shareIntent = Intent(Intent.ACTION_SEND)
                        shareIntent.type = "text/plain"
                        shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Bookgame")
                        var shareMessage = "\nDear friend, I am writing to inform you of a pleasant piece of news: Gamebook is now available in Play Store.\n\n"
                        shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n"
                        shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                        startActivity(Intent.createChooser(shareIntent, "choose one"))
                    } catch (ignored: Exception) {
                        ignored.printStackTrace()
                    }
                }
            }
            R.id.activity_settings_btn_back -> {
                onBackPressed()
            }
        }
    }

    private fun showCustomDialog() {
        val builder = AlertDialog.Builder(this)
        val dialogView = View.inflate(this, R.layout.custom_dialog, null)
        builder.setView(dialogView)
                .setView(dialogView)
                .setCancelable(false)
        val alert = builder.create()
        dialogView.findViewById<Button>(R.id.buy_button).setOnClickListener {
            try {
                purchaseProduct(purchases[2])
            } catch (e: Exception) {
                e.printStackTrace()
            } finally {
                alert.cancel()
            }
        }
        dialogView.findViewById<Button>(R.id.cancel_button).setOnClickListener {
            alert.cancel()
        }
        alert.show()
    }

    private fun animateClick(v: View) {
        when (v.id) {
            R.id.activity_settings_btn_restart,
            R.id.activity_settings_btn_menu,
            R.id.activity_settings_btn_change,
            R.id.activity_settings_btn_community,
            R.id.activity_settings_btn_good_deal,
            R.id.activity_settings_btn_endings,
            R.id.activity_settings_btn_recover,
            R.id.activity_settings_btn_share -> {
                val valueAnimator: ValueAnimator = if (resources.getBoolean(R.bool.isTablet))
                    ValueAnimator.ofFloat(TABLET_START_FONT_SIZE, TABLET_END_FONT_SIZE)
                else
                    ValueAnimator.ofFloat(SMART_START_FONT_SIZE, SMART_END_FONT_SIZE)
                valueAnimator.apply {
                    duration = 100
                    repeatCount = 1
                    repeatMode = REVERSE
                    addUpdateListener { updatedAnimation ->
                        (v as TextView).textSize = updatedAnimation.animatedValue as Float
                    }
                    start()
                }
            }
        }
    }

    @Throws(Exception::class)
    private fun purchaseProduct(product: InAppProduct) {
        val sku = product.sku
        val type = product.type
        val buyIntentBundle = myApplication.inAppService.getBuyIntent(
                3, packageName,
                sku, type, null)
        val pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT") as PendingIntent
        startIntentSenderForResult(pendingIntent.intentSender,
                REQUEST_CODE_BUY1, Intent(), 0, 0,
                0, null)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_BUY1) {
            val responseCode = data!!.getIntExtra("RESPONSE_CODE", -1)
            if (responseCode == BILLING_RESPONSE_RESULT_OK) {
                val purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA")
                readPurchase(purchaseData)
            }
        }
    }

    private fun readPurchase(purchaseData: String) {
        try {
            val jsonObject = JSONObject(purchaseData)
            when (jsonObject.getString("productId")) {
                PRODUCT3 -> {
                    Toast.makeText(baseContext, "##################", Toast.LENGTH_SHORT).show()
                    changeAnswer = true
                    getSharedPreferences(SHARED_KEY, Context.MODE_PRIVATE).edit().putBoolean(CHANGE_ANSWER, true).apply()
                    if (language == "ru") {
                        Toast.makeText(this, "Поздравляем!", Toast.LENGTH_SHORT).show()
                        Toast.makeText(this, "Вы открыли возможность изменять ответ", Toast.LENGTH_SHORT).show()
                    } else {
                        Toast.makeText(this, "Congratulations!", Toast.LENGTH_SHORT).show()
                        Toast.makeText(this, "You have opened the possibility to change choice", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        isPause = true
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out)
    }

    override fun onResume() {
        super.onResume()
        if (!isPause)
            (application as Application).changeThemeMusic(intent.getIntExtra("music", 1))
        isPause = false
    }

    public override fun onPause() {
        if (!isPause)
            (application as Application).stopMusic()
        super.onPause()
    }
}
