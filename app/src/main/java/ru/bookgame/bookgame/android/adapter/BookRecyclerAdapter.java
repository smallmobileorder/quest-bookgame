package ru.bookgame.bookgame.android.adapter;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ru.bookgame.bookgame.R;
import ru.bookgame.bookgame.android.Application;
import ru.bookgame.bookgame.android.activity.BookActivity;
import ru.bookgame.bookgame.android.activity.MainActivity;
import ru.bookgame.bookgame.android.viewholder.AnswerViewHolder;
import ru.bookgame.bookgame.android.viewholder.AutorCitatViewHolder;
import ru.bookgame.bookgame.android.viewholder.CitatViewHolder;
import ru.bookgame.bookgame.android.viewholder.EndViewHolder;
import ru.bookgame.bookgame.android.viewholder.HeaderViewHolder;
import ru.bookgame.bookgame.android.viewholder.MeViewHolder;
import ru.bookgame.bookgame.android.viewholder.NarativeViewHolder;
import ru.bookgame.bookgame.android.viewholder.OpponentViewHolder;
import ru.bookgame.bookgame.book.Answer;
import ru.bookgame.bookgame.book.BookProperty;
import ru.bookgame.bookgame.book.Conditional;
import ru.bookgame.bookgame.book.NestedConditional;
import ru.bookgame.bookgame.book.Paragraph;
import ru.bookgame.bookgame.book.Parameter;
import ru.bookgame.bookgame.parser.BookParser;

import static ru.bookgame.bookgame.parser.BookParser.COMPLICATED;
import static ru.bookgame.bookgame.parser.BookParser.COUNT;
import static ru.bookgame.bookgame.parser.BookParser.END;
import static ru.bookgame.bookgame.parser.BookParser.MULTIPLE_COMPLICATED;
import static ru.bookgame.bookgame.parser.BookParser.MY_TEXT;
import static ru.bookgame.bookgame.parser.BookParser.NARATIVE;
import static ru.bookgame.bookgame.parser.BookParser.OPPONENT_TEXT;
import static ru.bookgame.bookgame.parser.BookParser.QUESTION;
import static ru.bookgame.bookgame.parser.BookParser.RANDOM;
import static ru.bookgame.bookgame.parser.BookParser.REPEATING_QUESTION;
import static ru.bookgame.bookgame.parser.BookParser.SIMPLE;

public class BookRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int NARATIVE_TYPE = 1;
    private final int OPPONENT_TYPE = 2;
    private final int ME_TYPE = 3;
    private final int ANSWER_TYPE = 4;
    private final int END_TYPE = 5;
    private final int REPEATING_QUESTION_TYPE = 6;
    private final int HEADER_TYPE = 7;
    private final int CITAT_TYPE = 8;
    private final int AUTOR_CITAT = 9;

    private BookActivity bookActivity;

    private BookProperty bookProperty;

    private RecyclerView recyclerView;
    private List<Paragraph> currentParagraphsList;
    private short header;
    public String name = "";
    public int id = -1;

    public BookRecyclerAdapter(BookActivity activity, RecyclerView recyclerView, BookProperty bookProperty) {
        this.bookActivity = activity;
        this.bookProperty = bookProperty;
        this.recyclerView = recyclerView;
        init();
    }

    private void init() {
        if (bookProperty.getCurrentParagraphList() == null || bookProperty.getCurrentParagraphList().size() == 0) {
            bookProperty.setCurrentParagraphList(new ArrayList<>());
            currentParagraphsList = bookProperty.getCurrentParagraphList();
        } else {
            currentParagraphsList = bookProperty.getCurrentParagraphList();
            recyclerView.scrollToPosition(getItemCount() - 1);
        }
        if (currentParagraphsList.size() != 0)
            header = 3;
        else
            header = 0;
    }

    /*
    "Голос ЗА" - за обвиняемого = в пользу обвиняемого = НЕ ВИНОВЕН
     */

    public void nextParagraph() {
        if (header < 3) {
            Log.d("AGA_AGA", "Current - header: " + header);
            header++;
            notifyItemInserted(header - 1);
            return;
        }
        if (currentParagraphsList.size() == 0) {
            ((Application) bookActivity.getApplicationContext()).startMusic(R.raw.propusk, false);
            currentParagraphsList.add(bookProperty.getParagpraphs().get(1));
            Log.d("AGA_AGA", "Current - first paragraph: " + bookProperty.getParagpraphs().get(1).getId());
            notifyItemInserted(getItemCount());
            return;
        }

        Paragraph currentParagraph = currentParagraphsList.get(currentParagraphsList.size() - 1);

        if (currentParagraph.getType().equals(END)) {
            recyclerView.scrollToPosition(getItemCount() - 1);
            return;
        }

        Integer selectedAnswerId = currentParagraph.getSelectedAnswerId();
        if ((currentParagraph.getType().equals(QUESTION) || currentParagraph.getType().equals(REPEATING_QUESTION)) && selectedAnswerId == null) {
            recyclerView.scrollToPosition(getItemCount() - 1);
            return;
        }

        Integer nextParagraphID;

        if (currentParagraph.getType().equals(QUESTION) || currentParagraph.getType().equals(REPEATING_QUESTION)) {
            ((Application) bookActivity.getApplicationContext()).startMusic(R.raw.klik, false);
            Answer selectedAnswer = currentParagraph.getAnswers().get(selectedAnswerId);

            calcParameters(selectedAnswer);

            nextParagraphID = calcTransition(selectedAnswer);
            if (nextParagraphID == null) {
                nextParagraphID = selectedAnswer.getParagraphId();
            }
        } else if (currentParagraph.getDynamic() != null && currentParagraph.getDynamic()) {
            ((Application) bookActivity.getApplicationContext()).startMusic(R.raw.propusk, false);

            nextParagraphID = currentParagraph.getNextParagraphId();
            if (nextParagraphID == null && currentParagraph.getConditionalIds() != null) {
                nextParagraphID = calcNextParagraphByConditional(currentParagraph.getConditionalIds());
            }
        } else if (currentParagraph.getNextParagraphId() == null) {
            ((Application) bookActivity.getApplicationContext()).startMusic(R.raw.propusk, false);
            nextParagraphID = calcNextParagraphByConditional(currentParagraph.getConditionalIds());
        } else {
            ((Application) bookActivity.getApplicationContext()).startMusic(R.raw.propusk, false);
            nextParagraphID = currentParagraph.getNextParagraphId();
        }

        Paragraph nextParagraph;
        if (nextParagraphID != null)
            nextParagraph = bookProperty.getParagpraphs().get(nextParagraphID);
        else
            nextParagraph = bookProperty.getParagpraphs().get(currentParagraph.getId() + 1);


        //Скрываем для очередного REPEATING_QUESTION ответы, выбранные ранее
        if (nextParagraph.getType().equals(REPEATING_QUESTION)) {
            nextParagraph = BookParser.getParagraph(bookActivity, bookProperty, nextParagraph.getId());
            for (Paragraph tempParagraph : currentParagraphsList) {
                if (tempParagraph.getType().equals(REPEATING_QUESTION)) {
                    for (Answer tempAnswer : tempParagraph.getAnswers().values()) {
                        if (tempAnswer.isSelected()) {
                            if (nextParagraph.getAnswers().get(tempAnswer.getId()) != null) {
                                if (bookProperty.getFileResID() == R.raw.vtornik_rus)
                                    nextParagraph.getAnswers().get(tempAnswer.getId()).setSelected(false);
                                else
                                    nextParagraph.getAnswers().get(tempAnswer.getId()).setHide(true);
                            }
                        }
                    }
                }
            }
            for (Answer a : nextParagraph.getAnswers().values()) {
                if (a.getConditionalId() == null)
                    continue;
                a.setHide(calcConditionalToAnswer(a));
            }
        }

        //Скрываем для очередного QUESTION ответы, для которых не выполнились необходимые условия
        if (nextParagraph.getType().equals(QUESTION)) {
            nextParagraph = BookParser.getParagraph(bookActivity, bookProperty, nextParagraph.getId());
            for (Answer answer : nextParagraph.getAnswers().values()) {
                answer.setHide(calcConditionalToAnswer(answer));
            }
        }

        //Подсчитываем изменение параметров, относящихся к параграфу, при появлении очередного параграфа
        calcParameters(nextParagraph);

        //Подсчет параметров, которые внедряются в текст
        if (nextParagraph.getDynamic() != null && nextParagraph.getDynamic()) {
            calcVotes(nextParagraph);
        }

        if (nextParagraph.getType().equals(END)) {
            if (bookProperty.getOpenEnds() == null)
                bookProperty.setOpenEnds(new HashMap<>());
            bookProperty.getOpenEnds().put(nextParagraph.getEndedId(), nextParagraph);
        }
        if (currentParagraph.getAuthor() != null && nextParagraph.getAuthor() != null
                && currentParagraph.getAuthor().equals(nextParagraph.getAuthor()))
            nextParagraph.setHideAuthor(true);
        else
            nextParagraph.setHideAuthor(false);

        currentParagraphsList.add(nextParagraph);

        Log.d("AGA_AGA", "Current - " + currentParagraph.getId());
        Log.d("AGA_AGA", "Next - " + nextParagraph.getId());

        notifyItemInserted(getItemCount() - 1);
        recyclerView.smoothScrollToPosition(getItemCount() - 1);
    }

    @Override
    public void onViewAttachedToWindow(@NonNull RecyclerView.ViewHolder holder) {
        bookActivity.setTargetPosition(holder.getPosition());
        super.onViewAttachedToWindow(holder);
    }

    private boolean calcConditionalToAnswer(Answer answer) {
        if (answer.getConditionalId() == null)
            return false;
        Conditional conditional = bookProperty.getConditionals().get(answer.getConditionalId());
        return !calcConditional(conditional);
    }

    public void changeAnswer(Paragraph paragraph) {
        int positionParagraph = currentParagraphsList.indexOf(paragraph);
        notifyItemRangeRemoved(positionParagraph + header, getItemCount() - positionParagraph - header);

        List<Paragraph> tempList = currentParagraphsList.subList(0, positionParagraph + 1);
        Map<Integer, Paragraph> tempEnds = bookProperty.getOpenEnds();
        if (tempList.get(tempList.size() - 1).getType().equals(REPEATING_QUESTION)) {
            tempList.get(tempList.size() - 1).getAnswers().get(tempList.get(tempList.size() - 1).getSelectedAnswerId()).setSelected(false);
            tempList.get(tempList.size() - 1).setSelectedAnswerId(null);
        }
        bookProperty = new BookProperty(
                bookProperty.getFileResID(),
                bookProperty.getTitle(),
                bookProperty.getCitat(),
                bookProperty.getAutorCitat(),
                bookProperty.getBackgroundResID());
        BookParser.parse(bookActivity, bookProperty);
        init();
        this.header = 3;
        this.currentParagraphsList = tempList;
        if (tempList.get(tempList.size() - 1).getType().equals(QUESTION)) {
            tempList.set(tempList.size() - 1, bookProperty.getParagpraphs().get(tempList.get(tempList.size() - 1).getId()));
        }
        bookProperty.setCurrentParagraphList(tempList);
        bookProperty.setOpenEnds(tempEnds);
        recoverWeight();
        if (tempList.get(tempList.size() - 1).getType().equals(QUESTION))
            for (Answer a : tempList.get(tempList.size() - 1).getAnswers().values()) {
                a.setHide(calcConditionalToAnswer(a));
            }
        Log.d("AGA_AGA", "Add changed answer.");
        notifyItemInserted(getItemCount() - 1);
    }

    public void restartGame() {
        Map<Integer, Paragraph> tempEnds = bookProperty.getOpenEnds();
        bookProperty = new BookProperty(
                bookProperty.getFileResID(),
                bookProperty.getTitle(),
                bookProperty.getCitat(),
                bookProperty.getAutorCitat(),
                bookProperty.getBackgroundResID());
        BookParser.parse(bookActivity, bookProperty);
        bookProperty.setOpenEnds(tempEnds);
        init();
        notifyDataSetChanged();
    }

    private void recoverWeight() {
        for (Paragraph paragraph : currentParagraphsList) {
//            if (!paragraph.getType().equals(REPEATING_QUESTION) && bookProperty.getParagpraphs().containsValue(paragraph))
//                bookProperty.getParagpraphs().remove(paragraph.getId());

            if (paragraph.getType().equals(END))
                return;

            if (paragraph.getType().equals(QUESTION) || paragraph.getType().equals(REPEATING_QUESTION)) {
                if (paragraph.getSelectedAnswerId() != null) {
                    calcParameters(paragraph.getAnswers().get(paragraph.getSelectedAnswerId()));
                }
            }
            calcParameters(paragraph);
        }

    }

    private Integer calcTransition(Answer answer) {
        if (answer != null && answer.getTypeSegue() != null && answer.getTypeSegue().equals(RANDOM)) {
            if (new Random().nextInt(101) < 50) { //Типо вероятность 50:50
                return answer.getTransitions().get(0).getParagraphId();
            } else {
                return answer.getTransitions().get(1).getParagraphId();
            }
        } else {
            return null;
        }
    }

    private void calcParameters(Answer answer) {
        if (answer.getParameters() == null)
            return;
        for (Parameter p : answer.getParameters().values()) {
            int currentParameterID = p.getId();

            if (p.getOperation() == null)
                bookProperty.getParameters().get(currentParameterID).setWeight(
                        bookProperty.getParameters().get(currentParameterID).getWeight() + p.getWeight()
                );
            else switch (p.getOperation()) {
                case "+":
                    bookProperty.getParameters().get(currentParameterID).setWeight(
                            bookProperty.getParameters().get(currentParameterID).getWeight() + p.getWeight()
                    );
                    break;
                case "-":
                    bookProperty.getParameters().get(currentParameterID).setWeight(
                            bookProperty.getParameters().get(currentParameterID).getWeight() - p.getWeight()
                    );
                    break;
            }
        }
    }

    private void calcParameters(Paragraph paragraph) {
        if (paragraph.getParameters() == null)
            return;
        for (Parameter p : paragraph.getParameters().values()) {
            int currentParameterID = p.getId();
            if (bookProperty.getParameters().get(currentParameterID).getWeight() != null && p.getWeight() != null) {
                if (p.getOperation() == null)
                    bookProperty.getParameters().get(currentParameterID).setWeight(
                            bookProperty.getParameters().get(currentParameterID).getWeight() + p.getWeight()
                    );
                else switch (p.getOperation()) {
                    case "+":
                        bookProperty.getParameters().get(currentParameterID).setWeight(
                                bookProperty.getParameters().get(currentParameterID).getWeight() + p.getWeight()
                        );
                        break;
                    case "-":
                        bookProperty.getParameters().get(currentParameterID).setWeight(
                                bookProperty.getParameters().get(currentParameterID).getWeight() - p.getWeight()
                        );
                        break;
                }
            }
        }
    }

    private void calcVotes(Paragraph paragraph) {
        Pattern pattern;
        Matcher matcher;
        for (Parameter p : paragraph.getParameters().values()) {
            Parameter votesParameter = bookProperty.getParameters().get(p.getId());
            if (votesParameter.getConditional() == null)
                continue;
            pattern = Pattern.compile("~\\{" + votesParameter.getId() + "\\}");
            matcher = pattern.matcher(paragraph.getText());
            int count = 0;
            switch (votesParameter.getConditional()) {
                case "=":
                    for (int id : votesParameter.getParameter_ids()) {
                        if (bookProperty.getParameters().get(id).getWeight().equals(votesParameter.getWeight()))
                            count++;
                    }
                    break;
                case ">":
                    for (int id : votesParameter.getParameter_ids()) {
                        if (bookProperty.getParameters().get(id).getWeight() > votesParameter.getWeight())
                            count++;
                    }
                    break;
                case "<":
                    for (int id : votesParameter.getParameter_ids()) {
                        if (bookProperty.getParameters().get(id).getWeight() < votesParameter.getWeight())
                            count++;
                    }
                    break;
            }
            paragraph.setText(matcher.replaceAll(String.valueOf(count)));
        }
    }

    private Integer calcNextParagraphByConditional(List<Integer> conditionalIds) {
        try {
            for (int id : conditionalIds) {
                Conditional conditional = bookProperty.getConditionals().get(id);
                switch (conditional.getType()) {
                    case SIMPLE:
                        Boolean result = calcConditional(conditional);
                        if (result)
                            return conditional.getFirstParagraphId();
                        else
                            return conditional.getSecondParagraphId();
                    case COMPLICATED:
                        for (NestedConditional nestedConditional : conditional.getNestedConditionals()) {
                            if (nestedConditional.getSecond_value() == null) {
                                if (nestedConditional.getFirst_value()
                                        == calcConditional(bookProperty.getConditionals().get(nestedConditional.getFirst_id())))
                                    return nestedConditional.getParagraph_id();
                            } else {
                                if (nestedConditional.getFirst_value()
                                        == calcConditional(bookProperty.getConditionals().get(nestedConditional.getFirst_id()))
                                        && nestedConditional.getSecond_value()
                                        == calcConditional(bookProperty.getConditionals().get(nestedConditional.getSecond_id())))
                                    return nestedConditional.getParagraph_id();
                            }
                        }
                        break;
                    case COUNT:
                        int count = 0;
                        switch (conditional.getConditional()) {
                            case "=":
                                for (int idParam : conditional.getParameterIds()) {
                                    if (conditional.getValue().equals(bookProperty.getParameters().get(idParam).getWeight()))
                                        count++;
                                }
                                break;
                            case ">":
                                for (int idParam : conditional.getParameterIds()) {
                                    if (conditional.getValue() < bookProperty.getParameters().get(idParam).getWeight())
                                        count++;
                                }
                                break;
                            case "<":
                                for (int idParam : conditional.getParameterIds()) {
                                    if (conditional.getValue() > bookProperty.getParameters().get(idParam).getWeight())
                                        count++;
                                }
                                break;
                        }
                        if (conditional.getCountingValue() == null)
                            return conditional.getFirstParagraphId();
                        if (count > conditional.getCountingValue())
                            return conditional.getFirstParagraphId();
                        else
                            return conditional.getSecondParagraphId();
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }

    private Boolean calcConditional(Conditional conditional) {
        if (conditional == null)
            return null;
        if (!conditional.getType().equals(MULTIPLE_COMPLICATED)) {
            for (int i : conditional.getParameterIds()) {
                switch (conditional.getConditional()) {
                    case "=":
                        if (conditional.getValue().equals(bookProperty.getParameters().get(i).getWeight())) {
                            return true;
                        }
                        break;
                    case ">":
                        if (conditional.getValue() < bookProperty.getParameters().get(i).getWeight()) {
                            return true;
                        }
                        break;
                    case "<":
                        if (conditional.getValue() > bookProperty.getParameters().get(i).getWeight())
                            return true;
                        break;
                }
            }
        } else {
            switch (conditional.getConditional()) {
                case "||":
                    for (int i : conditional.getComplicatedIds()) {
                        if (calcConditional(bookProperty.getConditionals().get(i)))
                            return true;
                    }
                    break;
                case "&&":
                    for (int i : conditional.getComplicatedIds()) {
                        if (!calcConditional(bookProperty.getConditionals().get(i)))
                            return false;
                    }
                    return true;
                default:
                    for (int i : conditional.getComplicatedIds()) {
                        if (calcConditional(bookProperty.getConditionals().get(i)))
                            return true;
                    }
                    break;
            }
        }
        return false;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return HEADER_TYPE;
        if (position == 1)
            return CITAT_TYPE;
        if (position == 2)
            return AUTOR_CITAT;
        switch (getItem(position).getType()) {
            case NARATIVE:
                return NARATIVE_TYPE;
            case OPPONENT_TEXT:
                return OPPONENT_TYPE;
            case MY_TEXT:
                return ME_TYPE;
            case QUESTION:
                return ANSWER_TYPE;
            case END:
                return END_TYPE;
            case REPEATING_QUESTION:
                return REPEATING_QUESTION_TYPE;
        }
        return super.getItemViewType(position);
    }

    public Paragraph getItem(int position) {
        if (position == 0 || position == 1 || position == 2)
            return null;
        return currentParagraphsList.get(position - 3);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {
        View view;
        RecyclerView.ViewHolder viewHolder = null;
        switch (type) {
            case NARATIVE_TYPE:
                view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.recycler_item_narative, viewGroup, false);
                viewHolder = new NarativeViewHolder(view);
                break;
            case OPPONENT_TYPE:
                view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.recycler_item_opponent, viewGroup, false);
                viewHolder = new OpponentViewHolder(view);
                break;
            case ME_TYPE:
                view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.recycler_item_me, viewGroup, false);
                viewHolder = new MeViewHolder(view);
                break;
            case REPEATING_QUESTION_TYPE:
            case ANSWER_TYPE:
                view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.recycler_item_answer, viewGroup, false);
                viewHolder = new AnswerViewHolder(view, this);
                break;
            case END_TYPE:
                view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.recycler_item_end, viewGroup, false);
                viewHolder = new EndViewHolder(view, this);
                break;
            case HEADER_TYPE:
                view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.recycler_item_header, viewGroup, false);
                viewHolder = new HeaderViewHolder(view);
                break;
            case CITAT_TYPE:
                view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.recycler_item_citat, viewGroup, false);
                viewHolder = new CitatViewHolder(view);
                break;
            case AUTOR_CITAT:
                view = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.recycler_item_autor_citat, viewGroup, false);
                viewHolder = new AutorCitatViewHolder(view);
                break;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        switch (getItemViewType(position)) {
            case NARATIVE_TYPE:
                ((NarativeViewHolder) viewHolder).bind(getItem(position));
                break;
            case OPPONENT_TYPE:
                ((OpponentViewHolder) viewHolder).bind(getItem(position));
                break;
            case ME_TYPE:
                ((MeViewHolder) viewHolder).bind(getItem(position));
                break;
            case REPEATING_QUESTION_TYPE:
            case ANSWER_TYPE:
                ((AnswerViewHolder) viewHolder).bind(getItem(position));
                break;
            case END_TYPE:
                ((EndViewHolder) viewHolder).bind(getItem(position));
                break;
            case HEADER_TYPE:
                ((HeaderViewHolder) viewHolder).bind(bookProperty.getTitle());
                break;
            case CITAT_TYPE:
                ((CitatViewHolder) viewHolder).bind(bookProperty.getCitat());
                break;
            case AUTOR_CITAT:
                ((AutorCitatViewHolder) viewHolder).bind(bookProperty.getAutorCitat());
                break;
        }
    }

    @Override
    public int getItemCount() {
        return currentParagraphsList.size() + header;
    }

    public BookProperty getBookProperty() {
        return bookProperty;
    }


    public BookActivity getBookActivity() {
        return bookActivity;
    }

    public void showBuyingDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(bookActivity);
        View dialogView = View.inflate(bookActivity, R.layout.custom_dialog, null);
        builder.setView(dialogView)
                .setView(dialogView)
                .setCancelable(false);
        AlertDialog alert = builder.create();

        Button positiveButton = dialogView.findViewById(R.id.buy_button);
        positiveButton.setOnClickListener(v -> {
            try {
                bookActivity.purchaseProduct(MainActivity.Companion.getPurchases().get(2));
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                alert.cancel();
            }
        });
        Button negativeButton = dialogView.findViewById(R.id.cancel_button);
        negativeButton.setOnClickListener(v -> alert.cancel());
        alert.show();
    }


    public void openFinal() {
        bookActivity.openFinal();
    }

    public void clickSound() {
        bookActivity.ckickSound();
    }
}
