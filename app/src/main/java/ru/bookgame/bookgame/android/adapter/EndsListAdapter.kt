package ru.bookgame.bookgame.android.adapter

import android.animation.ObjectAnimator
import android.content.Context
import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

import ru.bookgame.bookgame.R
import ru.bookgame.bookgame.book.BookProperty
import ru.bookgame.bookgame.book.Paragraph
import ru.bookgame.bookgame.parser.BookParser

class EndsListAdapter(private val context: Context, private val openEnds: Map<Int, Paragraph>) : BaseAdapter() {

    override fun getCount(): Int {
        return openEnds.size
    }

    override fun getItem(position: Int): Any? {
        val sortList = openEnds.keys.toList().sorted()
        return openEnds[sortList[position]]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        var convertView = convertView
        if (convertView == null) {
            convertView = (context as AppCompatActivity).layoutInflater.inflate(R.layout.list_item_end, parent, false)
        } else {
            return convertView
        }
        val view = convertView

        object : AsyncTask<Void?, Void?, Void?>() {
            var stringBuilder1 = StringBuilder()
            var stringBuilder2 = StringBuilder()


            override fun doInBackground(vararg voids: Void?): Void? {
                stringBuilder1 = StringBuilder()
                stringBuilder1.append((getItem(position) as Paragraph).endedId)
                        .append(". ")
                        .append((getItem(position) as Paragraph).endedName)

                stringBuilder2 = StringBuilder()
                stringBuilder2.append(
                        BookParser.getParagraph(context,
                                BookProperty(context.intent.getIntExtra("file", -1), "", "", "", 1),
                                (getItem(position) as Paragraph).id!! - 2)!!.text
                ).append(" ")
                        .append(
                                BookParser.getParagraph(context,
                                        BookProperty(context.intent.getIntExtra("file", -1), "", "", "", 1),
                                        (getItem(position) as Paragraph).id!! - 1)!!.text
                        )
                return null
            }

            override fun onPostExecute(aVoid: Void?) {
                (view!!.findViewById<View>(R.id.list_item_end_text1) as TextView).text = stringBuilder1.toString()
                (view.findViewById<View>(R.id.list_item_end_text2) as TextView).text = stringBuilder2.toString()
                val animator = ObjectAnimator.ofFloat(view, "alpha", 0f, 1f)
                animator.duration = 500
                animator.addUpdateListener { animation -> view.alpha = animation.animatedValue as Float }
                animator.start()
            }
        }.execute()

        return view
    }
}
