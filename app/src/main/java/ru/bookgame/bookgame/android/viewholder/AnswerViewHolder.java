package ru.bookgame.bookgame.android.viewholder;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import ru.bookgame.bookgame.R;
import ru.bookgame.bookgame.android.activity.MainActivity;
import ru.bookgame.bookgame.android.adapter.BookRecyclerAdapter;
import ru.bookgame.bookgame.book.Answer;
import ru.bookgame.bookgame.book.Paragraph;

public class AnswerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


    private BookRecyclerAdapter adapter;
    private List<TextView> textViewList;
    private Paragraph paragraph;
    private List<TextView> visibleViews = new ArrayList<>();
    private List<Answer> visibleAnswers = new ArrayList<>();

    private ImageButton btnChange;

    public AnswerViewHolder(@NonNull View itemView, BookRecyclerAdapter adapter) {
        super(itemView);
        this.adapter = adapter;
        textViewList = new ArrayList<>();
        TextView textView1 = itemView.findViewById(R.id.answers_list_item_text1);
        textView1.setOnClickListener(this);
        TextView textView2 = itemView.findViewById(R.id.answers_list_item_text2);
        textView2.setOnClickListener(this);
        TextView textView3 = itemView.findViewById(R.id.answers_list_item_text3);
        textView3.setOnClickListener(this);
        TextView textView4 = itemView.findViewById(R.id.answers_list_item_text4);
        textView4.setOnClickListener(this);
        TextView textView5 = itemView.findViewById(R.id.answers_list_item_text5);
        textView5.setOnClickListener(this);
        TextView textView6 = itemView.findViewById(R.id.answers_list_item_text6);
        textView6.setOnClickListener(this);
        TextView textView7 = itemView.findViewById(R.id.answers_list_item_text7);
        textView7.setOnClickListener(this);
        TextView textView8 = itemView.findViewById(R.id.answers_list_item_text8);
        textView8.setOnClickListener(this);
        textViewList.add(textView1);
        textViewList.add(textView2);
        textViewList.add(textView3);
        textViewList.add(textView4);
        textViewList.add(textView5);
        textViewList.add(textView6);
        textViewList.add(textView7);
        textViewList.add(textView8);
        btnChange = itemView.findViewById(R.id.answers_list_item_btn_change);
        btnChange.setOnClickListener(this);
    }

    public void bind(Paragraph paragraph) {
        this.paragraph = paragraph;
        int i = 0;
        visibleViews.clear();
        visibleAnswers.clear();

        if (MainActivity.Companion.getChangeAnswer()) {
            if (adapter.getBookActivity().getChangeAnswerVisibility()) {
                if (paragraph.getSelectedAnswerId() == null) {
                    btnChange.setVisibility(View.INVISIBLE);
                    btnChange.setClickable(false);
                } else {
                    btnChange.setVisibility(View.VISIBLE);
                    btnChange.setClickable(true);
                }
            }else{
                btnChange.setVisibility(View.INVISIBLE);
            }
        }else{
            btnChange.setVisibility(View.INVISIBLE);
        }
        Collection<Answer> answersCollection = paragraph.getAnswers().values();
        for (Answer a : answersCollection) {
            textViewList.get(i).setText(a.getText());
            if (!a.isSelected()) {
                textViewList.get(i).setBackgroundResource(R.drawable.answer_back_unselected);
            }
            if (a.isHide()) {
                textViewList.get(i).setVisibility(View.GONE);
            } else {
                textViewList.get(i).setVisibility(View.VISIBLE);
                visibleViews.add(textViewList.get(i));
                visibleAnswers.add(a);
            }
            i++;
        }

        for (; i < textViewList.size(); i++) {
            textViewList.get(i).setVisibility(View.GONE);
        }

        i = 0;
        for (TextView tv : visibleViews) {
            if (visibleAnswers.get(i).isSelected()) {
                if (visibleViews.size() == 1)
                    tv.setBackgroundResource(R.drawable.answer_back_selected_around);
                else if (i == 0)
                    tv.setBackgroundResource(R.drawable.answer_back_selected_up);
                else if (i == visibleViews.size() - 1)
                    tv.setBackgroundResource(R.drawable.answer_back_selected_down);
                else
                    tv.setBackgroundResource(R.drawable.answer_back_selected);
            }
            i++;
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.answers_list_item_btn_change) {
            adapter.changeAnswer(adapter.getItem(getAdapterPosition()));
            adapter.getBookActivity().changeAnswerVisibility(false);
            v.setVisibility(View.INVISIBLE);
            adapter.notifyDataSetChanged();
            adapter.clickSound();
        } else {
            if (getAdapterPosition() != adapter.getItemCount() - 1) {
                adapter.nextParagraph();
                return;
            }
            String text = ((TextView) v).getText().toString();


            List<Answer> answersCollection = new ArrayList<>(paragraph.getAnswers().values());
            if (answersCollection.size() == 1) {
                answersCollection.get(0).setSelected(true);
                adapter.getItem(adapter.getItemCount() - 1).setSelectedAnswerId(answersCollection.get(0).getId());
            } else {
                for (Answer a : answersCollection) {
                    if (a.getText().equals(text)) {
                        a.setSelected(true);
                        adapter.getItem(adapter.getItemCount() - 1).setSelectedAnswerId(a.getId());
                        break;
                    }
                }
            }
            if (MainActivity.Companion.getChangeAnswer()) {
                if (adapter.getBookActivity().getChangeAnswerVisibility()) {
                    btnChange.setClickable(true);
                    btnChange.setVisibility(View.VISIBLE);
                }
            }
            bind(adapter.getItem(getAdapterPosition()));
            adapter.nextParagraph();
        }
    }
}
