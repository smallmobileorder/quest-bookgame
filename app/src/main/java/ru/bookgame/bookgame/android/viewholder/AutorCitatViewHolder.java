package ru.bookgame.bookgame.android.viewholder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import ru.bookgame.bookgame.R;

public class AutorCitatViewHolder extends RecyclerView.ViewHolder {

    private TextView headerView;

    public AutorCitatViewHolder(@NonNull View itemView) {
        super(itemView);
        headerView = itemView.findViewById(R.id.recycler_item_autor_citat_text);
    }

    public void bind(String header){
        headerView.setText(header);
    }
}
