package ru.bookgame.bookgame.android.viewholder;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.Locale;

import ru.bookgame.bookgame.BuildConfig;
import ru.bookgame.bookgame.R;
import ru.bookgame.bookgame.android.activity.BookActivity;
import ru.bookgame.bookgame.android.activity.MainActivity;
import ru.bookgame.bookgame.android.adapter.BookRecyclerAdapter;
import ru.bookgame.bookgame.book.Paragraph;

public class EndViewHolder extends RecyclerView.ViewHolder {

    private TextView openEndView;
    private StringBuilder stringBuilder;

    public EndViewHolder(@NonNull View itemView, BookRecyclerAdapter bookRecyclerAdapter) {
        super(itemView);
        String startNewGame;
        String y;
        String n;

        if (Locale.getDefault().getLanguage().equals("ru")) {
            startNewGame = "Начать заново?";
            y = "Да";
            n = "Нет";
        } else {
            startNewGame = "Start from scratch";
            y = "Yes";
            n = "No";
        }
        this.stringBuilder = new StringBuilder();
        Button newGameView = itemView.findViewById(R.id.recycler_item_end_new_game);
        newGameView.setOnClickListener(v -> {
            bookRecyclerAdapter.getBookActivity().ckickSound();
            new AlertDialog.Builder(bookRecyclerAdapter.getBookActivity()).setMessage(startNewGame)
                    .setPositiveButton(y, (dialog, which) -> {
                        bookRecyclerAdapter.restartGame();
                        bookRecyclerAdapter.getBookActivity().ckickSound();
                        bookRecyclerAdapter.getBookActivity().changeAnswerVisibility(false);
                    })
                    .setNegativeButton(n, (dialog, which) -> {
                        dialog.cancel();
                        bookRecyclerAdapter.getBookActivity().ckickSound();
                    })
                    .show();
        });
        Button changeAnswerView = itemView.findViewById(R.id.recycler_item_end_change_answer);
        changeAnswerView.setVisibility(View.VISIBLE);
        ImageView facebook = itemView.findViewById(R.id.facebook_icon_image);
        ImageView instagram = itemView.findViewById(R.id.instagram_icon_image);
        ImageView shareBtn = itemView.findViewById(R.id.share_icon_image);
        if (MainActivity.Companion.getChangeAnswer()) {
            changeAnswerView.setOnClickListener(v -> {
                bookRecyclerAdapter.getBookActivity().ckickSound();
                bookRecyclerAdapter.getBookActivity().changeAnswerVisibility(true);
                bookRecyclerAdapter.notifyDataSetChanged();
            });

        } else {
            changeAnswerView.setOnClickListener(v -> {
                bookRecyclerAdapter.getBookActivity().ckickSound();
                bookRecyclerAdapter.showBuyingDialog();
            });
        }
        openEndView = itemView.findViewById(R.id.recycler_item_end_open_end);
        String type = "image/*";
        String name = "";
        int id = -1;
        Log.i("SAVE_INSTAGRAM", bookRecyclerAdapter.getBookActivity().getBookId() + "");
        if (Locale.getDefault().getLanguage().equals("ru")) {
            switch (bookRecyclerAdapter.getBookActivity().getBookId()) {
                case R.raw.buro_rus:
                    name += "ss_buro_cover_rus";
                    id = R.drawable.ss_buro_cover_rus;
                    break;
                case R.raw.sud_rus:
                    name += "ss_sud_cover_rus";
                    id = R.drawable.ss_sud_cover_rus;
                    break;
                case R.raw.vtornik_rus:
                    name += "ss_vtornik_cover_rus";
                    id = R.drawable.ss_vtornik_cover_rus;
                    break;
                case R.raw.perevodchik_rus:
                    name += "ss_perevodchik_cover_rus";
                    id = R.drawable.ss_perevodchik_cover_rus;
                    break;
                case R.raw.den_rus:
                    name += "ss_praktika_cover_rus";
                    id = R.drawable.ss_praktika_cover_rus;
                    break;
            }
        } else {
            switch (bookRecyclerAdapter.getBookActivity().getBookId()) {
                case R.raw.buro_eng:
                    name += "ss_buro_cover_eng";
                    id = R.drawable.ss_buro_cover_eng;
                    break;
                case R.raw.sud_eng:
                    name += "ss_sud_cover_eng";
                    id = R.drawable.ss_sud_cover_eng;
                    break;
                case R.raw.vtornik_eng:
                    name += "ss_vtornik_cover_eng";
                    id = R.drawable.ss_vtornik_cover_eng;
                    break;
                case R.raw.perevodchik_eng:
                    name += "ss_perevodchik_cover_eng";
                    id = R.drawable.ss_perevodchik_cover_eng;
                    break;
                case R.raw.den_eng:
                    name += "ss_praktika_cover_eng";
                    id = R.drawable.ss_praktika_cover_eng;
                    break;
            }
        }
        bookRecyclerAdapter.getBookActivity().setImageData(name, id);
        bookRecyclerAdapter.getBookActivity().saveImage(name, id);
        BookActivity bookActivity = bookRecyclerAdapter.getBookActivity();
        instagram.setOnClickListener(v -> {
            if (!bookActivity.path.isEmpty()) {
                bookRecyclerAdapter.getBookActivity().ckickSound();
                createInstagramIntent(bookRecyclerAdapter, type, bookActivity.path);
            }
        });

        facebook.setOnClickListener(v -> {
            if (!bookActivity.path.isEmpty()) {
                bookRecyclerAdapter.getBookActivity().ckickSound();
                createFacebookIntent(bookRecyclerAdapter, type, bookActivity.path);
            }
        });
        shareBtn.setOnClickListener(v -> {
            if (Locale.getDefault().getLanguage().equals("ru")) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Книга Игр");
                Intent shareIntent1 = new Intent(Intent.ACTION_SEND);
                shareIntent1.setType("image/*");
                shareIntent1.putExtra(Intent.EXTRA_STREAM, bookActivity.path);
                shareIntent1.setPackage("com.instagram.android");
                Intent shareIntent2 = new Intent(Intent.ACTION_SEND);
                shareIntent2.setType("image/*");
                shareIntent2.putExtra(Intent.EXTRA_STREAM, bookActivity.path);
                shareIntent2.setPackage("com.facebook.katana");
                String shareMessage = "\nДорогой друг, я пишу тебе, чтобы сообщить преприятнейшее известие: Книга Игр теперь доступа в Play Store!\n\n";
                shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                Intent chooserIntent = Intent.createChooser(shareIntent, "Select Image");
                Parcelable[] intents = {shareIntent1, shareIntent2};
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intents);
                bookRecyclerAdapter.getBookActivity().startActivity(chooserIntent);
            } else {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, "Bookgame");
                Intent shareIntent1 = new Intent(Intent.ACTION_SEND);
                shareIntent1.setType("image/*");
                shareIntent1.putExtra(Intent.EXTRA_STREAM, bookActivity.path);
                shareIntent1.setPackage("com.instagram.android");
                Intent shareIntent2 = new Intent(Intent.ACTION_SEND);
                shareIntent2.setType("image/*");
                shareIntent2.putExtra(Intent.EXTRA_STREAM, bookActivity.path);
                shareIntent2.setPackage("com.facebook.katana");
                String shareMessage = "\nDear friend, I am writing to inform you of a pleasant piece of news: Gamebook is now available in Play Store.\n\n";
                shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                Intent chooserIntent = Intent.createChooser(shareIntent, "Select Image");
                Parcelable[] intents = {shareIntent1, shareIntent2};
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, intents);
                bookRecyclerAdapter.getBookActivity().startActivity(chooserIntent);
            }
        });
        openEndView.setOnClickListener(v -> {
            bookRecyclerAdapter.getBookActivity().ckickSound();
            bookRecyclerAdapter.openFinal();
        });

    }

    private void createInstagramIntent(BookRecyclerAdapter bookRecyclerAdapter, String type, String mediaPath) {
        if (checkPermission(bookRecyclerAdapter, 1011)) {
            // Create the new Intent using the 'Send' action.
            Intent share = new Intent(Intent.ACTION_SEND);
            // Set the MIME type
            share.setType(type);
            // Create the URI from the media
            File media = new File(mediaPath);
            Uri uri = Uri.fromFile(media);
            share.putExtra(Intent.EXTRA_STREAM, uri);
            share.setPackage("com.instagram.android");
            // Broadcast the Intent.
            BookActivity bookActivity = bookRecyclerAdapter.getBookActivity();
            if (share.resolveActivity(bookActivity.getPackageManager()) != null) {
                bookActivity.startActivity(Intent.createChooser(share, "Share to"));
            } else {
                bookActivity.showDialogApp("Instagram");
            }
        }
    }

    private boolean checkPermission(BookRecyclerAdapter bookRecyclerAdapter, int code) {
        if (ActivityCompat.checkSelfPermission(bookRecyclerAdapter.getBookActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(bookRecyclerAdapter.getBookActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    code);
            return false;
        } else return true;
    }


    private void createFacebookIntent(BookRecyclerAdapter bookRecyclerAdapter, String type, String mediaPath) {
        if (checkPermission(bookRecyclerAdapter, 1012)) {
            // Create the new Intent using the 'Send' action.
            Intent share = new Intent(Intent.ACTION_SEND);
            // Set the MIME type
            share.setType(type);
            // Create the URI from the media
            File media = new File(mediaPath);
            Uri uri = Uri.fromFile(media);
            // Add the URI to the Intent.
            share.putExtra(Intent.EXTRA_STREAM, uri);
            share.setPackage("com.facebook.katana");
            BookActivity bookActivity = bookRecyclerAdapter.getBookActivity();
            if (share.resolveActivity(bookActivity.getPackageManager()) != null) {
                bookActivity.startActivity(Intent.createChooser(share, "Share to"));
            } else {
                bookActivity.showDialogApp("Facebook");
            }
        }
    }

    public void bind(Paragraph paragraph) {
        String finale;
        if (Locale.getDefault().getLanguage().equals("ru")) {
            finale = "ФИНАЛ";
        } else {
            finale = "FINAL";
        }
        stringBuilder.delete(0, stringBuilder.length());
        stringBuilder.append(finale)
                .append("\n")
                .append(paragraph.getText());
        openEndView.setText(stringBuilder.toString());
    }

}
