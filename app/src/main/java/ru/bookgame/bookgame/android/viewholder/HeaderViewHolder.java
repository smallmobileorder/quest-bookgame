package ru.bookgame.bookgame.android.viewholder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import ru.bookgame.bookgame.R;

public class HeaderViewHolder extends RecyclerView.ViewHolder {

    private TextView headerView;

    public HeaderViewHolder(@NonNull View itemView) {
        super(itemView);
        headerView = itemView.findViewById(R.id.recyler_item_header_text);
    }

    public void bind(String header){
        headerView.setText(header);
    }
}
