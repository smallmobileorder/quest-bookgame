package ru.bookgame.bookgame.android.viewholder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import ru.bookgame.bookgame.R;
import ru.bookgame.bookgame.book.Paragraph;

public class MeViewHolder extends RecyclerView.ViewHolder {

    TextView textView;

    public MeViewHolder(@NonNull View itemView) {
        super(itemView);
        textView = itemView.findViewById(R.id.item_my_text);
    }

    public void bind(Paragraph paragraph) {
        textView.setText(paragraph.getText());
    }

}
