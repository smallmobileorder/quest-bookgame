package ru.bookgame.bookgame.android.viewholder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import ru.bookgame.bookgame.R;
import ru.bookgame.bookgame.book.Paragraph;

public class NarativeViewHolder extends RecyclerView.ViewHolder {

    TextView textView;

    public NarativeViewHolder(@NonNull View itemView) {
        super(itemView);
        textView = itemView.findViewById(R.id.item_narative_text);
    }

    public void bind(Paragraph paragraph){
        textView.setText(paragraph.getText());
    }

}
