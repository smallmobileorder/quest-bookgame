package ru.bookgame.bookgame.android.viewholder;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import ru.bookgame.bookgame.R;
import ru.bookgame.bookgame.book.Paragraph;

public class OpponentViewHolder extends RecyclerView.ViewHolder {

    TextView text, author;

    public OpponentViewHolder(@NonNull View itemView) {
        super(itemView);
        text = itemView.findViewById(R.id.item_opponent_text);
        author = itemView.findViewById(R.id.item_opponent_author);
    }

    public void bind(Paragraph paragraph) {
        if (paragraph.getAuthor() == null || paragraph.getAuthor().equals("") || (paragraph.getHideAuthor() != null && paragraph.getHideAuthor()))
            author.setVisibility(View.GONE);
        else {
            author.setVisibility(View.VISIBLE);
            author.setText(paragraph.getAuthor());
        }
        text.setText(paragraph.getText());
    }

}
