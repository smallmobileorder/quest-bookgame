package ru.bookgame.bookgame.book

import java.io.Serializable

class Answer(var id: Int?,
             var paragraphId: Int?,
             var text: String?,
             var typeSegue: String?,
             var conditionalId: Int?,
             var parameters: Map<Int, Parameter>?,
             var transitions: List<Transition>?,
             var isSelected: Boolean = false,
             var isHide: Boolean = false) : Serializable
