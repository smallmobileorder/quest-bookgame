package ru.bookgame.bookgame.book

import java.io.Serializable

class BookProperty(var fileResID: Int, var title: String, var citat: String, var autorCitat: String, var backgroundResID: Int) : Serializable {
    var paragpraphs: Map<Int, Paragraph>? = null
    var conditionals: Map<Int, Conditional>? = null
    var parameters: Map<Int, Parameter>? = null
    var openEnds: Map<Int, Paragraph>? = HashMap()
    var countEnds:Int? = null
    var currentParagraphList: List<Paragraph>? = null
}
