package ru.bookgame.bookgame.book

import java.io.Serializable

class Conditional(var id: Int?,
                  var value: Int?,
                  var type: String?,
                  var firstParagraphId: Int?,
                  var secondParagraphId: Int?,
                  var conditional: String?,
                  var countingValue: Int?,
                  var parameterIds: List<Int>?,
                  var complicatedIds: List<Int>?,
                  var nestedConditionals: List<NestedConditional>?):Serializable
