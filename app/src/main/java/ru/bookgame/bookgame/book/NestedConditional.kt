package ru.bookgame.bookgame.book

import java.io.Serializable

class NestedConditional(var paragraph_id: Int?,
                        var first_value: Boolean?,
                        var first_id: Int?,
                        var second_value: Boolean?,
                        var second_id: Int?):Serializable
