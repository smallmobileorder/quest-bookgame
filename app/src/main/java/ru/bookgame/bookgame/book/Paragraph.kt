package ru.bookgame.bookgame.book

import java.io.Serializable

class Paragraph(var id: Int?,
                var nextParagraphId: Int?,
                var type: String,
                var text: String?,
                var author: String?,
                var endedId: Int?,
                var endedName: String?,
                var dynamic: Boolean?,
                var hideAuthor: Boolean?,
                var answers: Map<Int, Answer>?,
                var parameters: Map<Int, Parameter>?,
                var conditionalIds: List<Int>?,
                var selectedAnswerId: Int? = null) : Serializable
