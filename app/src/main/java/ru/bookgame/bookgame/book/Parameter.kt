package ru.bookgame.bookgame.book

import java.io.Serializable

class Parameter(var id: Int?,
                var value: Int?,
                var weight: Int?,
                var name: String?,
                var type: String?,
                var operation: String?,
                var conditional: String?,
                var parameter_ids: List<Int>?) : Serializable
