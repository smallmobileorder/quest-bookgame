package ru.bookgame.bookgame.parser;

import android.content.Context;

import com.dd.plist.NSArray;
import com.dd.plist.NSDictionary;
import com.dd.plist.NSObject;
import com.dd.plist.PropertyListParser;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import ru.bookgame.bookgame.book.Answer;
import ru.bookgame.bookgame.book.BookProperty;
import ru.bookgame.bookgame.book.Conditional;
import ru.bookgame.bookgame.book.NestedConditional;
import ru.bookgame.bookgame.book.Paragraph;
import ru.bookgame.bookgame.book.Parameter;
import ru.bookgame.bookgame.book.Transition;

public class BookParser {

    //Глобальные ключи
    private static final String PARAGRAPHS = "Paragraphs";
    private static final String CONDITIONALS = "Conditionals";
    private static final String PARAMETERS = "Parameters";

    //Локальные ключи
    private static final String ID = "id";
    private static final String FIRST_ID = "first_id";
    private static final String FIRST_VALUE = "first_value";
    private static final String SECOND_ID = "second_id";
    private static final String SECOND_VALUE = "second_value";
    private static final String NEXT_ID = "next_paragraph_id";
    private static final String TYPE = "type";
    private static final String TYPE_SEGUE = "type_segue";
    private static final String TEXT = "text";
    private static final String PARAGRAPH_ID = "paragraph_id";
    private static final String FIRST_PARAGRAPH_ID = "first_paragraph_id";
    private static final String SECOND_PARAGRAPH_ID = "second_paragraph_id";
    private static final String COUNTING_VALUE = "counting_value";
    private static final String WEIGHT = "weight";
    private static final String ENDED_ID = "ended_id";
    private static final String ENDED_NAME = "ended_name";
    private static final String TRANSITIONS = "transitions"; //массив
    private static final String RANDOM_VALUE = "random_value";
    private static final String IS_DYNAMIC = "is_dynamic";
    private static final String AUTHOR = "author";
    private static final String VALUE = "value";
    private static final String NAME = "name";
    private static final String PARAMETER_IDS = "parameter_ids"; //массив
    private static final String CONDITIONAL_IDS = "conditional_ids"; //массив
    private static final String CONDITIONAL = "conditional";
    private static final String ANSWERS = "answers"; //массив
    private static final String pARAMETERS = "parameters"; //массив
    private static final String cONDITIONALS = "conditionals"; //массив
    private static final String OPERATION = "operation";
    private static final String CONDITIONAL_ID = "conditional_id";
    private static final String COMPLICATED_ID = "complicated_id"; //массив

    //Перечисления
    public static final String NARATIVE = "narative";
    public static final String MY_TEXT = "my_text";
    public static final String OPPONENT_TEXT = "opponent_text";
    public static final String QUESTION = "question";
    public static final String COUNT = "count";
    public static final String SIMPLE = "simple";
    public static final String COMPLICATED = "complicated";
    public static final String SINGLE = "single";
    public static final String RANDOM = "random";
    public static final String END = "end";
    public static final String REPEATING_QUESTION = "repeating_question";
    public static final String MULTIPLE_COMPLICATED = "multiple_complicated";

    public static void parse(Context context, BookProperty bookProperty) {
        try {
            InputStream is = context.getResources().openRawResource(bookProperty.getFileResID());
            NSDictionary rootDict = (NSDictionary) PropertyListParser.parse(is);

            NSObject[] paragraphs = ((NSArray) rootDict.objectForKey(PARAGRAPHS)).getArray();
            bookProperty.setParagpraphs(getParagraphs(paragraphs));

            NSObject[] parameters = ((NSArray) rootDict.objectForKey(PARAMETERS)).getArray();
            bookProperty.setParameters(getParameters(parameters));

            NSObject[] conditionals = ((NSArray) rootDict.objectForKey(CONDITIONALS)).getArray();
            bookProperty.setConditionals(getConditionals(conditionals));

            int count = 0;
            for (Paragraph p : bookProperty.getParagpraphs().values()) {
                if (p.getType().equals(END))
                    count++;
            }
            bookProperty.setCountEnds(count);

        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Check ID");
        }
    }

    public static Paragraph getParagraph(Context context, BookProperty bookProperty, int id) {
        try {
            InputStream is = context.getResources().openRawResource(bookProperty.getFileResID());
            NSDictionary rootDict = (NSDictionary) PropertyListParser.parse(is);
            NSObject[] paragraphs = ((NSArray) rootDict.objectForKey(PARAGRAPHS)).getArray();
            Integer curID;
            for (NSObject obj : paragraphs) {
                curID = (Integer) getJavaObject(obj, ID);
                if (curID == id) {
                    return new Paragraph(
                            id,
                            (Integer) getJavaObject(obj, NEXT_ID),
                            (String) getJavaObject(obj, TYPE),
                            (String) getJavaObject(obj, TEXT),
                            (String) getJavaObject(obj, AUTHOR),
                            (Integer) getJavaObject(obj, ENDED_ID),
                            (String) getJavaObject(obj, ENDED_NAME),
                            (Boolean) getJavaObject(obj, IS_DYNAMIC),
                            false,
                            getAnswersByParagraphs(obj),
                            getParametersByParagraphs(obj),
                            getConditionalIDS(obj),
                            null
                    );
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //type - narative, question, my_text, opponent_text, repeating_question, end
    private static Map<Integer, Paragraph> getParagraphs(NSObject[] paragraphs) {
        Map<Integer, Paragraph> result = new HashMap<>();
        for (NSObject paragraphDict : paragraphs) {
            Integer id = (Integer) getJavaObject(paragraphDict, ID);

            Paragraph paragraph = new Paragraph(
                    id,
                    (Integer) getJavaObject(paragraphDict, NEXT_ID),
                    (String) getJavaObject(paragraphDict, TYPE),
                    (String) getJavaObject(paragraphDict, TEXT),
                    (String) getJavaObject(paragraphDict, AUTHOR),
                    (Integer) getJavaObject(paragraphDict, ENDED_ID),
                    (String) getJavaObject(paragraphDict, ENDED_NAME),
                    (Boolean) getJavaObject(paragraphDict, IS_DYNAMIC),
                    false,
                    getAnswersByParagraphs(paragraphDict),
                    getParametersByParagraphs(paragraphDict),
                    getConditionalIDS(paragraphDict),
                    null
            );
            result.put(id, paragraph);
        }
        return result;
    }

    private static Map<Integer, Parameter> getParametersByParagraphs(NSObject dict) {
        Map<Integer, Parameter> result = new HashMap<>();
        NSObject nsObject = ((NSDictionary) dict).objectForKey(pARAMETERS);
        if (nsObject == null)
            return null;
        NSObject[] parameters = ((NSArray) nsObject).getArray();
        for (NSObject parameterDict : parameters) {
            Integer id = (Integer) getJavaObject(parameterDict, ID);
            Parameter parameter = new Parameter(
                    id,
                    (Integer) getJavaObject(parameterDict, VALUE),
                    (Integer) getJavaObject(parameterDict, WEIGHT),
                    (String) getJavaObject(parameterDict, NAME),
                    (String) getJavaObject(parameterDict, TYPE),
                    (String) getJavaObject(parameterDict, OPERATION),
                    (String) getJavaObject(parameterDict, CONDITIONAL),
                    getParameterIDS(parameterDict)
            );
            result.put(id, parameter);
        }
        return result;
    }

    private static Map<Integer, Answer> getAnswersByParagraphs(NSObject dict) {
        Map<Integer, Answer> result = new HashMap<>();
        NSObject nsObject = ((NSDictionary) dict).objectForKey(ANSWERS);
        if (nsObject == null)
            return null;
        NSObject[] answers = ((NSArray) nsObject).getArray();
        for (int i = answers.length - 1; i >= 0; i--) {
            NSObject answerDict = answers[i];
            Integer id = (Integer) getJavaObject(answerDict, ID);
            Answer answer = new Answer(
                    id,
                    (Integer) getJavaObject(answerDict, PARAGRAPH_ID),
                    (String) getJavaObject(answerDict, TEXT),
                    (String) getJavaObject(answerDict, TYPE_SEGUE),
                    (Integer) getJavaObject(answerDict, CONDITIONAL_ID),
                    getParametersByAnswer(answerDict),
                    getTransitionsByAnswer(answerDict),
                    false,
                    false
            );
            result.put(id, answer);
        }
        return result;
    }

    private static Map<Integer, Parameter> getParametersByAnswer(NSObject dict) {
        Map<Integer, Parameter> result = new HashMap<>();
        NSObject nsObject = ((NSDictionary) dict).objectForKey(pARAMETERS);
        if (nsObject == null)
            return null;
        NSObject[] parameters = ((NSArray) nsObject).getArray();
        for (NSObject parameterDict : parameters) {
            Integer id = (Integer) getJavaObject(parameterDict, ID);
            Parameter parameter = new Parameter(
                    id,
                    (Integer) getJavaObject(parameterDict, VALUE),
                    (Integer) getJavaObject(parameterDict, WEIGHT),
                    (String) getJavaObject(parameterDict, NAME),
                    (String) getJavaObject(parameterDict, TYPE),
                    (String) getJavaObject(parameterDict, OPERATION),
                    (String) getJavaObject(parameterDict, CONDITIONAL),
                    getParameterIDS(parameterDict)
            );
            result.put(id, parameter);
        }
        return result;
    }

    private static List<Transition> getTransitionsByAnswer(NSObject dict) {
        List<Transition> result = new ArrayList<>();
        NSObject nsObject = ((NSDictionary) dict).objectForKey(TRANSITIONS);
        if (nsObject == null)
            return null;
        NSObject[] transitions = ((NSArray) nsObject).getArray();
        for (NSObject transitionDict : transitions) {
            Transition transition = new Transition(
                    (Integer) getJavaObject(transitionDict, RANDOM_VALUE),
                    (Integer) getJavaObject(transitionDict, PARAGRAPH_ID)
            );
            result.add(transition);
        }
        return result;
    }

    //type - count
    private static Map<Integer, Parameter> getParameters(NSObject[] parameters) {
        Map<Integer, Parameter> result = new HashMap<>();
        for (NSObject parameterDict : parameters) {
            Integer id = (Integer) getJavaObject(parameterDict, ID);

            Parameter parameter = new Parameter(
                    id,
                    (Integer) getJavaObject(parameterDict, VALUE),
                    (Integer) getJavaObject(parameterDict, WEIGHT),
                    (String) getJavaObject(parameterDict, NAME),
                    (String) getJavaObject(parameterDict, TYPE),
                    (String) getJavaObject(parameterDict, OPERATION),
                    (String) getJavaObject(parameterDict, CONDITIONAL),
                    getParameterIDS(parameterDict)
            );
            result.put(id, parameter);
        }
        return result;
    }

    //type - simple, count, complicated, multiple_complicated
    private static Map<Integer, Conditional> getConditionals(NSObject[] conditionals) {
        Map<Integer, Conditional> result = new HashMap<>();
        for (NSObject conditionalDict : conditionals) {
            Integer id = (Integer) getJavaObject(conditionalDict, ID);

            Conditional conditional = new Conditional(
                    id,
                    (Integer) getJavaObject(conditionalDict, VALUE),
                    (String) getJavaObject(conditionalDict, TYPE),
                    (Integer) getJavaObject(conditionalDict, FIRST_PARAGRAPH_ID),
                    (Integer) getJavaObject(conditionalDict, SECOND_PARAGRAPH_ID),
                    (String) getJavaObject(conditionalDict, CONDITIONAL),
                    (Integer) getJavaObject(conditionalDict, COUNTING_VALUE),
                    getParameterIDS(conditionalDict),
                    getComplicatedIDS(conditionalDict),
                    getConditionalByConditional(conditionalDict)
            );
            result.put(id, conditional);
        }
        return result;
    }

    private static List<Integer> getComplicatedIDS(NSObject conditionalDict) {
        List<Integer> complicatedId = new ArrayList<>();
        NSObject nsObject = ((NSDictionary) conditionalDict).objectForKey(COMPLICATED_ID);
        if (nsObject == null)
            return null;
        NSObject[] ids = ((NSArray) nsObject).getArray();
        for (NSObject id : ids) {
            complicatedId.add((Integer) id.toJavaObject());
        }
        return complicatedId;
    }

    private static List<NestedConditional> getConditionalByConditional(NSObject dict) {
        List<NestedConditional> result = new ArrayList<>();
        NSObject nsObject = ((NSDictionary) dict).objectForKey(cONDITIONALS);
        if (nsObject == null)
            return null;
        NSObject[] conditionals = ((NSArray) nsObject).getArray();
        for (NSObject conditionalDict : conditionals) {
            NestedConditional nestedConditional = new NestedConditional(
                    (Integer) getJavaObject(conditionalDict, PARAGRAPH_ID),
                    (Boolean) getJavaObject(conditionalDict, FIRST_VALUE),
                    (Integer) getJavaObject(conditionalDict, FIRST_ID),
                    (Boolean) getJavaObject(conditionalDict, SECOND_VALUE),
                    (Integer) getJavaObject(conditionalDict, SECOND_ID)
            );
            result.add(nestedConditional);
        }
        return result;
    }

    private static List<Integer> getParameterIDS(NSObject dict) {
        List<Integer> result = new ArrayList<>();
        NSObject nsObject = ((NSDictionary) dict).objectForKey(PARAMETER_IDS);
        if (nsObject == null)
            return null;
        NSObject[] ids = ((NSArray) nsObject).getArray();
        for (NSObject i : ids) {
            result.add((Integer) i.toJavaObject());
        }
        return result;
    }

    private static List<Integer> getConditionalIDS(NSObject dict) {
        List<Integer> result = new ArrayList<>();
        NSObject nsObject = ((NSDictionary) dict).objectForKey(CONDITIONAL_IDS);
        if (nsObject == null)
            return null;
        NSObject[] ids = ((NSArray) nsObject).getArray();
        for (NSObject i : ids) {
            result.add((Integer) i.toJavaObject());
        }
        return result;
    }

    private static Object getJavaObject(NSObject dict, String key) {
        NSDictionary currentDict = (NSDictionary) dict;
        NSObject nsObject = currentDict.objectForKey(key);
        if (nsObject == null)
            return null;
        else
            return nsObject.toJavaObject();
    }
}
